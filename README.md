# plant-watering-server-wemos-lolin32

Plant watering server with WeMos-LOLIN32 (esp32)

<img src="/20200413-175512.png" alt="Titelbild"/>

<p>  </p>

<img src="/20200413-175622.png" alt="Titelbild"/>

<p>  </p>

===> 17.05.2020-13:00 <===

Betrieb des ESP32 als Access Point (AP) integriert. Bisher wurde der Pflanzen-Server ausschließlich als Station (ST) betrieben,
das setzt auf jeden Fall einen Router voraus, über den die Kommunikation der Pflanzen-Clienten mit dem Planzen-Server läuft.
Allerdings stehen Planzen im Garten eventuell so weit vom Router entfernt, das eine Verbindung zum Router nicht mehr gewährleistet ist. 
Für diese Fälle wurde der Betrieb des Pflanzen-Servers als Acces Point (AP) integriert. Der normale Betrieb läuft damit ohne Router, einfach
mit dem Smartphone durchgeführt werden, indem der AP des Servers ausgewählt wird. Nur für die Aktualisierung der NTP-Zeit wird eine Verbindung zum Internet bebötigt. Dafür muss man mit dem Smartphone einen mobilen HotSpot öffnen. Ist "#define UseRtcDS1307" eingstellt, wird von da an selbst bei einem Spannungsausfall die richtige Zeit verwendet.

Der Betrieb als Access Point (AP) wird mit "#define UseSoftAP" in "setup_daten.h" aktiviert. In "ssid_password.h" werden die entsprechenden Zugänge zum ESP-AP und zum Internet (NTP-Zeit) eingestellt.

===> 13.05.2020-22:00 <===

RTC_DS1307 integriert. 
Kann mit "#define UseRtcDS1307" in "setup_daten.h" aktiviert werden.
Wenn beim Neustart des Servers die NTP-Zeit nicht geholt werden kann, wird die Zeit des DS1307 verwendet.  

===> 19.04.2020-12:00 <===

KiCAD Schaltung und Platine

===> 13.04.2020-19:00 <===

Ansteuerung LEDs, OpenSCAD - Gehäuse

===> 27.03.2020-16:00 <===

Kommentare in die Sources eingepflegt. Notwendige Fleißarbeit ;-)

===> 26.03.2020-17:30 <===

Mein PflanzenServer auf Basis des esp32 ist im Testbetrieb, zwar noch auf Steckbrett 
läuft aber wirklich gut. 


