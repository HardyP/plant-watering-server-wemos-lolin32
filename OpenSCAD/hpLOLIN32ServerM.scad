// Abwandlung von http://www.thingiverse.com/thing:1695285
// Customizable Hammond Style Box by stylesuxx 
// http://creativecommons.org/licenses/by-nc/3.0

use <hpPlatine.scad>

/* [Hidden] */
//$fn=36;
$fn=120;
print=true;
platine=false;
wandhalterung=true;

/* [Type] */
// Which part of the enclosure to view?
part="lid"; // [both,body,lid,test]
// Which type of box do you want?
type="VonInnen"; // [VonInnen,VonAussen]

// Abmessungen
Breite=18*2.54+3; 
Laenge=38*2.54+3;
Hoehe=32;
DeckelHoehe=20;
WandDicke=2;
snapWidth=1.00; // Snap im Deckel
outerDiameter=6;

// fuer magnetisch 
verschluss="m";
postDiameter=0;
postWidthBody=7;
postWidthLid=7;
holeWidthBody=4.5;
holeHeightBody=4.3;
holeWidthLid=4.5;
holeHeightLid=4.3;
xOffsetHole=2.5;
yOffsetHole=2.5;
zOffsetHole=0.0;
zOffsetPost=-0.3;

// ende magnetisch

// fuer schrauben
/*
verschluss="s";
postDiameter=0;
postWidthBody=7;
postWidthLid=7;
holeDiameterBody=2.2;
holeDiameterLid=3.2;
holeHeadDiameterLid=6.5;
holeHeadHeight=2.5;
xOffsetHole=6;
yOffsetHole=6;
zOffsetHole=0;
zOffsetPost=-0.3;
*/
// ende schrauben

enclosure(type,part);
 
module enclosure(type,part,boxColor) 
{
    if (type == "VonInnen") VonInnen(part,boxColor);
    if (type=="VonAussen") VonAussen(part,boxColor);
}

module VonInnen(part="both",boxColor="blue") 
{
    outerLength=Laenge+WandDicke*2;
    outerWidth=Breite+WandDicke*2;
    outerHeight=Hoehe+WandDicke*2;
    lidHeight=DeckelHoehe;
    innerLength=Laenge;
    innerWidth=Breite;
    innerHeight=Hoehe;
    hammond(outerLength,outerWidth,outerHeight,lidHeight,innerLength,innerWidth,innerHeight,part,boxColor);
}

module VonAussen(part="both",boxColor="blue") 
 {
    outerLength=Laenge;
    outerWidth=Breite;
    outerHeight=Hoehe;
    lidHeight=DeckelHoehe;
    innerLength=Laenge-WandDicke*2;
    innerWidth=Breite-WandDicke*2;
    innerHeight=Hoehe-WandDicke*2;
    hammond(outerLength,outerWidth,outerHeight,lidHeight,innerLength,innerWidth,innerHeight,part,boxColor);
}

module hammond(outerLength,outerWidth,outerHeight,lidHeight,innerLength,innerWidth,innerHeight,part,boxColor)
{
    postRadius=postDiameter/2;
    outerRadius=outerDiameter/ 2;
    bodyHeight=outerHeight-lidHeight;
    xOffset=(outerLength-innerLength)/2;
    yOffset=(outerWidth-innerWidth)/2;
    zOffset=(outerHeight-innerHeight)/2;
    screwPosts=[ [0,0,zOffset],[outerLength,0,zOffset],[outerLength,outerWidth,zOffset],[0,outerWidth,zOffset]];
    translate([outerWidth/2,-outerHeight/2,outerLength]) 
    {
        rotate([0,90,90]) 
        {
            if(part == "both" && !print) 
            {
                translate([outerLength,0,outerHeight]) rotate([0,180,0]) color(boxColor) renderBody();
                %renderLid();
            }
            if (part=="both" && print)
            {
                yOffset=(outerLength+outerWidth)/2;
                pos=[outerLength,yOffset,0];
                rot=[90,0,270];
                translate(pos) rotate(rot)
                {
                    renderBody();
                    translate([0,outerWidth +3,0]) renderLid();
                }
            }
            if (part=="body")
            {
                yOffset=(outerLength+outerWidth)/2;
                pos=(print) ? [outerLength,yOffset,0] : [outerLength,0,outerHeight];
                rot=(print) ? [90,0,270] : [0,180,0];
                translate(pos) rotate(rot) color(boxColor) renderBody();
            }
            if (part=="lid")
            {
                yOffset=(outerLength+outerWidth)/2;
                pos=(print) ? [outerLength,yOffset,0] : [0,0,0];
                rot=(print) ? [90,0,270] : [0,0,0];
                translate(pos) rotate(rot) color(boxColor) renderLid();
            }
            if (part=="test") 
            {
                length=13;
                width=13;
                height=30;
                translate([outerLength-outerHeight,0,outerLength/4]) rotate([0,90,0]) 
                {
                    intersection() 
                    {
                        translate([0,0,outerHeight-height]) cube([length,width,height]);
                        translate([outerLength,0,outerHeight]) rotate([0,180,0]) renderBody();
                    }
                    translate([0,-outerWidth+ (length*2),0])
                    intersection()
                    {
                        translate([0,outerWidth-width,outerHeight-height]) cube([length,width,height]);
                        translate([outerLength,0,outerHeight]) rotate([0,180,0]) renderLid();
                    }
                }
            }
        }
    }
  
    module renderBody() 
    {
        postHeight=bodyHeight-zOffset+zOffsetPost;
        // Hier moegliche Ausschnitte im Body-Boden testen       
        // translate([30,18,0]) cube([5,5,10],true);

        //  
        difference() 
        {
            difference() 
            {
                roundBody(bodyHeight);
                difference() 
                {
                    translate([xOffset,yOffset,zOffset]) cube([innerLength,innerWidth,bodyHeight]);
                    screwPosts(postWidthBody,postHeight);
                }
            }
            // Hier moegliche Ausschnitte im Body-Boden         
            // translate([30,18,0]) cube([5,5,10],true);
            // translate([50,18,0]) cylinder(d=5.5,h=10);
            //

            // Bohrungen fuer LEDs
            LED1D=5.5;
            LED1X=5*2.54;
            LED1Y=7*2.54;
            LED1Z=11.0;
            LED2D=5.5;
            LED2X=0;
            LED2Y=7*2.54;
            LED2Z=11.0;
            LED3D=5.5;
            LED3X=-5*2.54;
            LED3Y=7*2.54;
            LED3Z=11.0;
            translate([Laenge/2+WandDicke-LED1X-1.27,Breite/2+WandDicke-LED1Y+1.27,WandDicke]) 
                cylinder(h=LED1Z,d=LED1D,center=true); 
            translate([Laenge/2+WandDicke-LED2X-1.27,Breite/2+WandDicke-LED2Y+1.27,WandDicke])
                cylinder(h=LED2Z,d=LED2D,center=true); 
            translate([Laenge/2+WandDicke-LED3X-1.27,Breite/2+WandDicke-LED3Y+1.27,WandDicke]) 
                cylinder(h=LED3Z,d=LED3D,center=true); 
            //
            // Ausschnitt Stecker
            translate([Laenge+WandDicke,Breite/2-WandDicke+1.5,Hoehe-DeckelHoehe-1]) color("red") 
                rotate([0,0,0])  
                cube([WandDicke*4,12,8],true);
            //
            // Wand Steckerseite duenner
            translate([Laenge+WandDicke-3,Breite/2+WandDicke,Hoehe-DeckelHoehe-1])
                color("red") 
                rotate([0,0,0])  
                cube([WandDicke*4,34.7,8],true);
            //

            if (verschluss=="m") mpostHoles(holeHeightBody,holeWidthBody,postHeight-holeHeightBody+zOffsetHole);
            if (verschluss=="s") 
            {
                holeRadiusBody=holeDiameterBody/2;
                spostHoles(outerHeight-lidHeight,holeRadiusBody,zOffsetHole);     
            }
        }
        // Hier moegliche Posts fuer Platinen etc im Body-Boden einfuegen
        // xy ab Innenkante
        //    holePost(x=20,y=20,height=6,diaout=5,diahole=2.5);
        //     holePost(x=20+30,y=20,height=6,diaout=5,diahole=2.5);
        //
    }

    module renderLid() 
    {
        zOffsetPost=0.1;
        postHeight=lidHeight-zOffset+zOffsetPost;
        // Hier moegliche Ausschnitte im Deckel testen   
        //        translate([0,0,0]) cube([5,5,10],true);
        //translate([(innerLength/2),25,0]) cube([62,21,10],true);

        //  
        difference() 
        {
            group() 
            {
                difference() 
                    {
                        roundBody(lidHeight);
                        translate([xOffset,yOffset,zOffset]) cube([innerLength,innerWidth,lidHeight]);
                    }
                translate([0,0,-zOffsetPost]) screwPosts(postWidthLid,postHeight);       
                translate([(outerLength-innerLength)/2+postWidthBody,yOffset,zOffset])
                    cube([innerLength-postWidthBody-postWidthBody,snapWidth,lidHeight-zOffset+1.5]);
                translate([(outerLength-innerLength)/2+postWidthBody,outerWidth-yOffset-snapWidth,zOffset])
                    cube([innerLength-postWidthBody-postWidthBody,snapWidth,lidHeight-zOffset+1.5]);
                translate([yOffset,(outerWidth-innerWidth)/2+postWidthBody,zOffset])
                    cube([snapWidth,innerWidth-postWidthBody-postWidthBody,lidHeight-zOffset+1.5]);
                translate([outerLength-yOffset-snapWidth,(outerWidth-innerWidth)/2+postWidthBody,zOffset])
                    cube([snapWidth,innerWidth-postWidthBody-postWidthBody,lidHeight-zOffset+1.5]);
            }
            BLD=1.5;
            // Belueftungsloecher Seite
            for(BLX=[0:1:2])
            {
            translate([Laenge-WandDicke-5-BLX*6,Breite/2+WandDicke,WandDicke*2.5]) 
                rotate([0,90,90]) cylinder(d=BLD, h=60,center=true);
            translate([Laenge-WandDicke-5-BLX*6,Breite/2+WandDicke,WandDicke*2.5+6]) 
                rotate([0,90,90]) cylinder(d=BLD, h=60,center=true);
            translate([Laenge-WandDicke-5-BLX*6,Breite/2+WandDicke,WandDicke*2.5+12]) 
                rotate([0,90,90]) cylinder(d=BLD, h=60,center=true);
            }
            for(BLX=[0:1:1])
            {
            translate([Laenge-WandDicke-8-BLX*6,Breite/2+WandDicke,WandDicke*2.5+3]) 
                rotate([0,90,90]) cylinder(d=BLD, h=60,center=true);
            translate([Laenge-WandDicke-8-BLX*6,Breite/2+WandDicke,WandDicke*2.5+9]) 
                rotate([0,90,90]) cylinder(d=BLD, h=60,center=true);

            }
            // Belueftungsloecher Unten
            for(BLY=[0:1:2])
            {
                 translate([WandDicke-16,Breite/2+WandDicke,WandDicke*2.5+BLY*6]) 
                    rotate([0,90,0]) cylinder(d=BLD, h=30);
                 translate([WandDicke-16,Breite/2+WandDicke+6,WandDicke*2.5+BLY*6]) 
                    rotate([0,90,0]) cylinder(d=BLD, h=30);
                 translate([WandDicke-16,Breite/2+WandDicke+12,WandDicke*2.5+BLY*6]) 
                    rotate([0,90,0]) cylinder(d=BLD, h=30);
                 translate([WandDicke-16,Breite/2+WandDicke+6,WandDicke*2.5+BLY*6]) 
                    rotate([0,90,0]) cylinder(d=BLD, h=30);
                 translate([WandDicke-16,Breite/2+WandDicke+12,WandDicke*2.5+BLY*6]) 
                    rotate([0,90,0]) cylinder(d=BLD, h=30);
                 translate([WandDicke-16,Breite/2+WandDicke-6,WandDicke*2.5+BLY*6]) 
                    rotate([0,90,0]) cylinder(d=BLD, h=30);
                 translate([WandDicke-16,Breite/2+WandDicke-12,WandDicke*2.5+BLY*6]) 
                    rotate([0,90,0]) cylinder(d=BLD, h=30);
            }
            for(BLY=[0:1:1])
            {
            translate([WandDicke-16,Breite/2+WandDicke-3,WandDicke*2.5+3+BLY*6]) 
                rotate([0,90,0]) cylinder(d=BLD, h=30);
             translate([WandDicke-16,Breite/2+WandDicke+3,WandDicke*2.5+3+BLY*6]) 
                rotate([0,90,0]) cylinder(d=BLD, h=30);
             translate([WandDicke-16,Breite/2+WandDicke-9,WandDicke*2.5+3+BLY*6]) 
                rotate([0,90,0]) cylinder(d=BLD, h=30);

             translate([WandDicke-16,Breite/2+WandDicke+9,WandDicke*2.5+3+BLY*6]) 
                rotate([0,90,0]) cylinder(d=BLD, h=30);
           } 
           // Wandhalterung
           if(wandhalterung) 
            {
                WandhalterungAussen(Laenge-15,Breite/2);
                WandhalterungAussen(30,Breite/2);
            }
 
            //

            if (verschluss=="m") mpostHoles(holeHeightLid,holeWidthLid,postHeight-holeHeightLid+zOffsetHole);
            if(verschluss=="s") 
            {   
                holeHeadRadius=holeHeadDiameterLid/2;
                holeRadiusLid=holeDiameterLid/2;
                spostHoles(outerHeight,holeRadiusLid,zOffsetHole-WandDicke+holeHeadHeight/2);     
                postHolesSenkung(holeHeadHeight,holeHeadRadius,holeRadiusLid, zOffsetHole-WandDicke+holeHeadHeight/2);  
            }
        }
        // Wandhalterung
        if(wandhalterung) 
        {
            WandhalterungInnen(Laenge-15,Breite/2);
            WandhalterungInnen(30,Breite/2);
        }
        // Platine
        if (platine) {translate([Laenge/2+WandDicke,Breite/2+WandDicke,WandDicke+8+1.6]) color("lightblue") rotate([0,0,0]) Platine();}
         // Post fuer Platine
         holePost(x=Laenge/2-13*2.54,y=Breite/2-7*2.54-1.27,height=8,diaout=7,diahole=2.2);
         holePost(x=Laenge/2+13*2.54,y=Breite/2-7*2.54-1.27,height=8,diaout=7,diahole=2.2);
         holePost(x=Laenge/2-13*2.54,y=Breite/2+7*2.54+1.27,height=8,diaout=7,diahole=2.2);
         holePost(x=Laenge/2+13*2.54,y=Breite/2+7*2.54+1.27,height=8,diaout=7,diahole=2.2);
    }

    module roundBody(cutHeight) 
    {
        cornersBottom=[
            [outerRadius,outerRadius,outerRadius],
            [outerLength-outerRadius,outerRadius,outerRadius],
            [outerLength-outerRadius,outerWidth-outerRadius,outerRadius],
            [outerRadius,outerWidth-outerRadius,outerRadius]];
        cornersTop=[
            [outerRadius,outerRadius,outerRadius],
            [outerLength-outerRadius,outerRadius,outerRadius],
            [outerLength-outerRadius,outerWidth-outerRadius,outerRadius],
            [outerRadius,outerWidth-outerRadius,outerRadius]];
        bodyInnerWidth=outerWidth-outerRadius*2;
        bodyInnerLength=outerLength-outerRadius*2;
        bodyInnerHeight=cutHeight- outerRadius;
        translate([0,outerRadius,outerRadius]) cube([outerLength,bodyInnerWidth,bodyInnerHeight]);
        translate([outerRadius,0,outerRadius]) cube([bodyInnerLength,outerWidth, bodyInnerHeight]);
        for (pos=cornersTop) translate(pos) cylinder(r=outerRadius,h=bodyInnerHeight);
        difference() 
        {
            hull() for (pos=cornersBottom) translate(pos) sphere(r=outerRadius);
            translate([-1,-1,cutHeight]) cube([outerLength+2,outerWidth+2,cutHeight]);
        }
    }

    module mpostHoles(height,width,zOffset) 
    {
        for(i=[0:3]) translate(screwPosts[i]) rotate([0,0,90*i])
        {
            // translate([xOffsetHole,yOffsetHole,zOffset])  cube([width,width,height],false);
            // translate([xOffsetHole+width/2,yOffsetHole+width/2,zOffset])  cylinder(h=height,d=width,center=false);
            translate([xOffsetHole+width/2,yOffsetHole+width/2,zOffset-(5*0.12)])  cylinder(h=height,d=width,center=false);
        }
    }

    module spostHoles(height,radius,zOffset) 
    {
        // echo(height=height,radius=radius,zOffset=zOffset);  
        for(i=[0:3]) translate(screwPosts[i]) rotate([0,0,90*i]) translate([xOffsetHole,yOffsetHole,zOffset]) cylinder(r=radius,h=height);
    }

    module postHolesSenkung(height,radius1,radius2,zOffset) 
    {
        // echo(height=height,radius1=radius1,radius2=radius2,zOffset=zOffset);  
        for(i=[0:3]) 
            translate(screwPosts[i]) rotate([0,0,90*i])
            //  translate([xOffsetHole,yOffsetHole,-0.41])
            translate([xOffsetHole,yOffsetHole,zOffset]) cylinder(h=height,r1=radius1,r2=radius2,center=true);
    }

    module WandhalterungInnen(x,y)
    {
        translate([x+WandDicke,y+WandDicke,0])
        {
            difference() 
            {
                hull()
                    {
                         translate([0,0,WandDicke]) cylinder(d=9.6+4,h=6);
                         translate([0-15,0,WandDicke]) cylinder(d=9.6+4,h=6);
                    }
               color("red",1) hull()
               {
                     translate([0,0,WandDicke]) cylinder(d=9.6,h=5);
                     translate([-15,0,WandDicke]) cylinder(d=9.6,h=5);
                }
            }
        }
    }

    module WandhalterungAussen(x,y)
    {
        translate([x+WandDicke,y+WandDicke,])
        {
           color("red",1) hull()
            {
                 translate([0,0,0]) cylinder(d=5,h=WandDicke);
                 translate([-15,0,0]) cylinder(d=5,h=WandDicke);
            }
             translate([-15,0,0]) color("red",1) cylinder(d=9.6,h=WandDicke);
        }
    }

    module holePost(x,y,height,diaout,diahole) 
    {
        translate([x+WandDicke,y+WandDicke,height/2+WandDicke])
        {
            difference() 
            {
                cylinder(r=diaout/2,h=height,center=true);
                cylinder(r=diahole/2,h=height,center=true);
            }
        }
    } 

    module screwPosts(postWidth,height) 
    {
        for(i=[0:3]) translate(screwPosts[i]) rotate([0,0,90*i]) screwPost(postWidth,height);
    }

    module screwPost(postWidth,height) 
    {
        offsetRadius=postWidth-postRadius;
        translate([min(xOffset,yOffset),min(xOffset,yOffset),0]) 
        {
            cube([postWidth,postWidth-postRadius,height]);
            cube([postWidth-postRadius,postWidth ,height]);
            translate([offsetRadius,offsetRadius,0]) cylinder(r=postRadius,h=height);
        }
    }
}