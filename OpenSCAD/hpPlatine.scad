XA=38*2.54; 
YA=19*2.54; 
ZA=1.6; 
PE=3*2.54; // Pfosten in den Ecken 
BPD=3.5; // Befestigung Platine Durchmesser
BPX=6*2.54; // Befestigung Platine vom Rand X
BPY=2*2.54; // Befestigung Platine vom Rand Y
LOLINX=11*2.54;
LOLINY=0;
LOLINZ=2.54;
SDX=32*2.54;
SDY=0;
SDZ=1.27;
LED1X=5*2.54;
LED1Y=-7*2.54;
LED1Z=17.0;
LED2X=0;
LED2Y=-7*2.54;
LED2Z=17.0;
LED3X=-5*2.54;
LED3Y=-7*2.54;
LED3Z=17.0;

$fn=18;

module Platine()
{
    difference()
    {
        union()
        {
            translate([-XA/2+LOLINX+8*2.54,LOLINY,ZA/2+LOLINZ-2.54-1.27]) rotate([0,0,0]) import("PCB_x38y17.stl",convexity=1);  
            translate([-XA/2+LOLINX+1.27,LOLINY,LOLINZ+8.4+0.2]) rotate([0,0,0]) import("WEMOS_LOLIN32_V1_Stift_Model.stl",convexity=1); 
            translate([-XA/2+LOLINX+9*2.54,LOLINY-5*2.54+1.27,ZA+2.54]) rotate([0,180,0]) import("hpBL_A20H84L25.stl",convexity=1); 
            translate([-XA/2+LOLINX+9*2.54,LOLINY+4*2.54+1.27,ZA+2.54]) rotate([0,180,0]) import("hpBL_A16H84L25.stl",convexity=1); 
            translate([-XA/2+SDX+1.27,SDY+1.27,ZA+SDZ+8.4+0.2]) rotate([0,0,90]) import("D1_MINI_RTC+MicroSD_Stift_Model.stl",convexity=1);
            translate([-XA/2+SDX+5*2.54,SDY+1.27+5*2.54,ZA+2.54]) rotate([0,180,90]) import("hpBL_A08H84L25.stl",convexity=1);
            translate([-XA/2+SDX-4*2.54,SDY+1.27+5*2.54,ZA+2.54]) rotate([0,180,90]) import("hpBL_A08H84L25.stl",convexity=1);

  
            translate([LED1X+1.27,LED1Y+1.27,ZA/2+LED1Z]) rotate([0,0,0]) import("hpLED5-20_Model.stl",convexity=1);  
            translate([LED2X+1.27,LED2Y+1.27,ZA/2+LED2Z]) rotate([0,0,0]) import("hpLED5-20_Model.stl",convexity=1);  
            translate([LED3X+1.27,LED3Y+1.27,ZA/2+LED3Z]) rotate([0,0,0]) import("hpLED5-20_Model.stl",convexity=1);  
        }
        union()
        {
            // Ecken
            translate([XA/2-PE/2,YA/2-PE/2,0]) cube([PE,PE,ZA*2],center=true);
            translate([-XA/2+PE/2,YA/2-PE/2,0]) cube([PE,PE,ZA*2],center=true);
            translate([XA/2-PE/2,-YA/2+PE/2,0]) cube([PE,PE,ZA*2],center=true);
            translate([-XA/2+PE/2,-YA/2+PE/2,0]) cube([PE,PE,ZA*2],center=true);
            // Befestigungslöcher
            translate([XA/2-BPX,YA/2-BPY,0]) cylinder(h=ZA*2,d=BPD,center=true);
            translate([-XA/2+BPX,YA/2-BPY,0]) cylinder(h=ZA*2,d=BPD,center=true);
            translate([XA/2-BPX,-YA/2+BPY,0]) cylinder(h=ZA*2,d=BPD,center=true);
            translate([-XA/2+BPX,-YA/2+BPY,0]) cylinder(h=ZA*2,d=BPD,center=true);

        }
    }
}

Platine();








   
