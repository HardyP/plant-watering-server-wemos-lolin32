const char* router_ssid="router_ssid";  // Router fuer Zugriff aufs Internet
const char* router_password="router_password"; // Router fuer Zugriff aufs Internet
#ifdef UseSoftAP
const char* server_ssid="server_ssid"; // ssid des AccessPoints (WiFi_AP)
const char* server_password="server_password"; // password des AccessPoints (WiFi_AP)
#else
const char* server_ssid="server_ssid"; // Bei WiFi-Sta identisch mit router_ssid
const char* server_password="server_password"; // Bei WiFi-Sta identisch mit router_password
#endif
