#include <Arduino.h>
#include <WiFi.h>
#include <WebServer.h>
#include <WiFiUdp.h>
#include <TimeLib.h> // https://github.com/PaulStoffregen/Time
#include <SPI.h>
#include <SdFat.h>
#include "sdios.h"
#include <setup_daten.h>
#include <../../ssid_password.h> // Pfad, ssid und password anpassen

#define NTP_SERVER "de.pool.ntp.org"
#define TZ_INFO "WEST-1DWEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00" // Western European Time

// Connect the SD card to the following pins:
//-------------------------------------------
// SD Card | ESP32  ! Lolin32
//    CS       SS        5
//    DI       MOSI      23
//    CLK      SCK       18
//    DO       MISO      19
//    VDD      3.3V
//    GND      GND
  
const uint8_t chipSelect = 5; // SD card chip select pin.
int Green_LED=17; 
int Core0_LED=16; 
int Core1_LED=15; 
const int PacketSizeMax=120;
bool SerialDEBUG=false;

typedef struct 
{
char SensorKennung[20];
char Alias[31]="";
time_t Zeit;
char tZeit[18];
time_t WasserZeit;
char tWasserZeit[10];
uint16_t Feuchte=0;
uint16_t MinFeuchte=0;
float Temperatur=0;
float Spannung=0;
uint16_t SchlafZeit=0;
uint16_t WartenBisWasser=0;
uint16_t WartenBisWasserS=0;
uint16_t PumpZeit=0;
uint16_t Wasser=0;
uint16_t Wasser12Stunden=0;
uint16_t Wasser1Tage=0;
uint16_t Wasser2Tage=0;
uint16_t Wasser3Tage=0;
uint16_t Wasser7Tage=0;
uint16_t Wasser14Tage=0;
uint16_t Wasser30Tage=0;
float TemperaturMax12Stunden=0;
float TemperaturMax1Tage=0;
float TemperaturMax2Tage=0;
float TemperaturMax3Tage=0;
float TemperaturMax7Tage=0;
float TemperaturMax14Tage=0;
float TemperaturMax30Tage=0;
float TemperaturMin12Stunden=0;
float TemperaturMin1Tage=0;
float TemperaturMin2Tage=0;
float TemperaturMin3Tage=0;
float TemperaturMin7Tage=0;
float TemperaturMin14Tage=0;
float TemperaturMin30Tage=0;
float TemperaturD12Stunden=0;
float TemperaturD1Tage=0;
float TemperaturD2Tage=0;
float TemperaturD3Tage=0;
float TemperaturD7Tage=0;
float TemperaturD14Tage=0;
float TemperaturD30Tage=0;
} ServerTaskRecord;

typedef struct 
{
char SensorKennung[20];
char SensorSetup[PacketSizeMax];
} SensorTaskRecord;

TaskHandle_t SensorTask;
TaskHandle_t ServerTask;
TaskHandle_t SensorLedTask;
TaskHandle_t FadingGreenLedTask;
TaskHandle_t SetupBlinkLedTask;
QueueHandle_t QueueToSensorTask;
QueueHandle_t QueueToServerTask;
ServerTaskRecord ServerTaskDaten[SensorenMax];
SensorTaskRecord SensorTaskDaten[SensorenMax];

char PacketFromSensorTask[PacketSizeMax]; // Empfangspuffer von QueueToServerTask  
char PacketFromServerTask[PacketSizeMax]; // Empfangspuffer von QueueToSensorTask 
char PacketToSensorTask[PacketSizeMax]; // Sendepuffer von QueueToSensorTask  
char incomingPacket[PacketSizeMax]; // Puffer für eingehende Pakete

esp_sleep_wakeup_cause_t wakeup_reason;
const int WiFiRetryMax=100;
time_t NTP_now=0;
time_t Zeit;
float Spannung;
uint16_t Feuchte;
uint16_t MinFeuchte;
uint16_t Wasser;
uint16_t SchlafZeit;
uint16_t WartenBisWasser;
uint16_t WartenBisWasserS;
uint16_t PumpZeit;
String Alias;
float Temperatur;
float TemperaturD12StundenCount;
float TemperaturD1TageCount;
float TemperaturD2TageCount;
float TemperaturD3TageCount;
float TemperaturD7TageCount;
float TemperaturD14TageCount;
float TemperaturD30TageCount;

char buffer[256];
char LineBuffer[256];
char CopyBuf[512]; 
String SensorKennung;
String LineString;
String SaveLineString;
String tmp;
int CopyBufLen;
struct tm local;
String sServerZeit;

int ServerTaskSelSens=0; // selektierter Sensor 
int ServerTaskAnzSens=0; // Anzahl erkannter Sensoren

// Variable fuer SensorTask
// ------------------------------------------------------------
time_t ST_TimeLastUDP=0;
time_t ST_DiffTimeLastUDP=0;
int ST_AnzTimeLastUDP=0;
uint16_t ST_ExtraSchlafZeit;
int ST_SelSens=0; // selektierter Sensor 
int ST_AnzSens=0; // Anzahl erkannter Sensoren
char ST_SensorKennung[20];
char ST_SensorSetup[PacketSizeMax];
String ST_sPacketFromServerTask;
String ST_sSensorKennung;
String ST_sSensorSetup;
String ST_sIncomingPacket;
// ------------------------------------------------------------

WebServer server(80);
WiFiUDP Udp;
SdFat sd;
SdFile ChirpFile;
SdFile ChirpBackupFile;
SdFile ChirpSollWerteFile;
SdFile VorlageFile;
SdFile WebSiteFile;
SdFile WebSiteClientSetupFile;
SdFile StreamFile; 
SdFile SourceFile;
SdFile DestFile;

const char* ChirpFileName="/chirpdaten.txt";
const char* VorlageFileName="/vorlage.html";
const char* WebSiteFileName="/index.html";
const long MaxClients=SensorenMax;
const time_t Sekunden12h=12*60*60;
const time_t Sekunden1d=Sekunden12h*2;
const time_t Sekunden2d=Sekunden1d*2;
const time_t Sekunden3d=Sekunden1d*3;
const time_t Sekunden7d=Sekunden1d*7;
const time_t Sekunden14d=Sekunden1d*14;
const time_t Sekunden30d=Sekunden1d*30;
char ChirpBackupFileName[20];
char WebSiteClientSetupFileName[20]; 
long SetClientNr=MaxClients+1;
uint32_t FileSize;
String StreamPath;
String StreamDataType;
char StreamBuf[1024]; 
int StreamBufLen;

char HeadStr[6000];
char TabStr[60000];

#ifdef UseRtcDS1307
#include <Wire.h> 
#include <RtcDS1307.h>
// CONNECTIONS DS1307:
// DS1307 SDA --> SDA/21
// DS1307 SCL --> SCL/22
// DS1307 VCC --> 5v
// DS1307 GND --> GND
RtcDS1307<TwoWire> DS1307(Wire);
//time_t TimeDS1307;
#endif

#include <Utility.h>
#include <ServerZeitHolen.h>
#include <FileSystemStarten.h>
#include <ServerTaskDatenAnalyse.h>
#include <WebSiteErzeugen.h>
#include <ServerTask.h>
#include <SensorTask.h>  



void print_wakeup_reason()
{
  switch(wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_EXT0 : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1 : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER : Serial.println("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : Serial.println("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.printf("Wakeup was not caused by deep sleep: %d\n",wakeup_reason); break;
  }
}
   
void setup()
{
  time_t isTime=0;
  wakeup_reason = esp_sleep_get_wakeup_cause(); // wakeup Ursache holen
  setenv("TZ",TZ_INFO,1); // Zeitzone  muss nach dem reset neu eingestellt werden
  tzset();
  pinMode(Green_LED,OUTPUT); 
  pinMode(Core0_LED,OUTPUT); 
  pinMode(Core1_LED,OUTPUT); 
  Intro(); // "Utility.h" 
  // Ab geht es ...
  Serial.begin(115200);
  delay(200);
  ggfDEBUG(); // "Utility.h" ... Durch Eingabe an der Konsole in der Wartezeit, werden zusätzliche DEBUG-Infos ausgegeben
  delay(200);
  Serial.println(" \r\n");
  Serial.println("!===============================!");
  Serial.println("!  hpLolin32 PflanzenServer V4  !"); 
  Serial.println("!===============================!");
  Serial.println("");
  Serial.println("WakeupReason: " + String(wakeup_reason));
 	print_wakeup_reason(); // Print the wakeup reason for ESP32
  #ifdef UseRtcDS1307
  DS1307_Starten();
  #endif 
 
  if (wakeup_reason==0)  // Wenn wakeup durch Reset
  {
    ggfNeueServerZeit(); // "ServerZeitHolen.h" ... Es wird versucht mit die aktuelle NTP-Zeit vom Server zu holen
  } 
  else
  {
    // Zeit der internen RTC des esp32 wird verwendet
    time(&isTime);
    setTime(isTime);
    Serial.print("Systemzeit: ");
    PrintDateTime(now());
    Serial.println();
  }
  StartSetupBlinkLed();
  FileSystemStarten(); // "FileSystemStarten.h" ... Das Filesystem auf der SD-Karte wird gestartet
  LokalNetStarten(); // "Utility.h" ... Das lokale WiFi-Netz wird gestaret. Einstellungen in "setup_daten.h"
  ServerTaskDatenAnalyse(); // "ServerTaskDatenAnalyse.h" ... Die Clientendaten der letzten 30Tage werden aufbereitet 
  WebSiteHeadErzeugen(); // "WebSiteErzeugen.h" ... Es wird der Head der lokalen Website erszeugt und im "HeadStr" gespeichert
  WebSiteErzeugen(); // "WebSiteErzeugen.h" ... Es wird die "/index.html" auf der SD-Karte erzeugt.
  
  server.on("/",HTTP_GET,handleIndex); // Handle für "/index.html"
  server.on("/ClientSetup",HTTP_GET,handleClientSetup); // Handle zur Konfiguration der einzelnen Clienten

  server.begin();
  Serial.println("HTTP Server wurde gestartet!");
  if (Udp.begin(localUdpPort)) {Serial.println("UDP wurde gestartet!");} else {Serial.println("UDP konnte nicht gestartet werden!");}
  #ifdef UseSoftAP
  IPAddress SoftAP_IP=WiFi.softAPIP();
  Serial.printf("Nun lauschen auf IP %s, UDP-Port %d\n", SoftAP_IP.toString().c_str(), localUdpPort);
  #else
  Serial.printf("Nun lauschen auf IP %s, UDP-Port %d\n", WiFi.localIP().toString().c_str(), localUdpPort);
  #endif
  StopSetupBlinkLed();
 
  //  Core0 ist für den Datenaustausch zwischen Server und den einzelnen Clienten zuständig, dazu wird UDP verwendet.
  //  Die einzelnen schicken ein Datenpaket an den Server mit den Messwerten. Der Server sendet als Antwort als Empfangsquittung 
  //  an den betreffenden Clienten zurück, ggf mit neuen Setup-Daten (zB. Schlafzeit etc.). Da sich Core0 lediglich um diesen Datenverkehr
  //  kümmert, sind seine Reaktionen sehr schnell, und die Clienten nur kurz mit dem Server verbunden.
  //  Die Loop des Core0 ist: LoopSensorTask in "SensorTask.h".
  //
  //  Core1 ist für die Aufbereitung der Daten, Erzeugung der WebSite und den lokalen Server zuständig. Er arbeitet vollkommen asynchron zum
  //  Datenverkehr des Core0 mit den Clienten.
  //  Die Loop des Core1 ist: LoopServerTask in "ServerTask.h".
  //
  //  Core0 und Core1 sind über zwei Queue verbunden.
  //  QueueToServerTask: Datenverkehr von SensorTask (Core0) zu ServerTask (Core1). Über diese Queue sendet der SensorTask die von 
  //  Clienten empfangenen Daten an den ServerTask zur weiteren Verarbeitung.
  //  QueueToSensorTask: Datenverkehr von ServerTask (Core1) zu SensorTask (Core0). Über diese Queue sendet der ServerTask die SetUp-Daten
  //  an den SensorTask, dieser überträgt sie dann an den betreffenden Clienten, wenn der sich das nächste mal meldet. 
  QueueToServerTask=xQueueCreate(40,sizeof(incomingPacket));
  if(QueueToServerTask==NULL) {Serial.println("Fehler bei Erzeugung der QueueToServerTask");}
  QueueToSensorTask=xQueueCreate(40,sizeof(incomingPacket));
  if(QueueToSensorTask==NULL) {Serial.println("Fehler bei Erzeugung der QueueToSensorTask");}
  xTaskCreatePinnedToCore(LoopSensorTask,"SensorTask",10000,NULL,10,&SensorTask,0);
  xTaskCreatePinnedToCore(LoopServerTask,"ServerTask",10000,NULL,5,&ServerTask,1);
  xTaskCreatePinnedToCore(LoopFadingGreenLedTask,"FadingGreenLedTask",10000,NULL,1,&FadingGreenLedTask,1);
}
 
void loop()
{
    vTaskDelete (NULL);
}