
void PrintDateTime(time_t DateTime)
{
  // Formatierte Ausgabe von Datum und Zeit
  strftime(buffer,20,"%d.%m.%Y-%H:%M",localtime(&DateTime));
  Serial.print(buffer); 
}
 
#ifdef UseRtcDS1307
void DS1307_Starten()  
{
  Serial.println("RTC-DS1307 starten");
  DS1307.Begin();
  if (!DS1307.IsDateTimeValid()) 
  {
    if (DS1307.LastError()!=0)
    {
      Serial.print("RTC-DS1307 communications error = ");
      Serial.println(DS1307.LastError());
    }
    else {Serial.println("RTC lost confidence in the DateTime!");}
  }
  if (!DS1307.GetIsRunning()) {DS1307.SetIsRunning(true);}
  DS1307.SetSquareWavePin(DS1307SquareWaveOut_Low); 
  Serial.print("RTC-DS1307-Zeit: ");
  PrintDateTime(DS1307.GetDateTime());
}
#endif

bool getNTPtime(int NTP_RetryMax) 
{
  // Versuche sich mit dem NTP-Server zu synchronisieren
  int NTP_Retry=0;
  NTP_now=0;
  do
  {
    digitalWrite(Green_LED,LOW); delay(200); digitalWrite(Green_LED,HIGH); delay(400);                      
    digitalWrite(Green_LED,LOW); delay(200); digitalWrite(Green_LED,HIGH); delay(400);                      
    digitalWrite(Green_LED,LOW); delay(200); digitalWrite(Green_LED,HIGH); delay(400);                      
    digitalWrite(Green_LED,LOW); delay(200); digitalWrite(Green_LED,HIGH); delay(400);                      
    NTP_Retry++;
    time(&NTP_now);
    localtime_r(&NTP_now,&local);
    Serial.print("Retry: ");
    Serial.print(NTP_Retry);
    Serial.print(" --> NTP: ");
    Serial.println(NTP_now);
  } while ((NTP_now<1581600000)&&(NTP_Retry<=NTP_RetryMax)); 
  if(NTP_Retry>=NTP_RetryMax) 
  {
    NTP_now=0;
    return false;
  }
  Serial.print("NTP - Systemzeit: ");
  PrintDateTime(NTP_now);
  Serial.println("");
  return true;
}
 
bool ServerZeitHolen()
{
  // Es wird versucht die NTP-Zeit vom Zeitserver zu holen. 
  Serial.println(" \r\n");
  Serial.println("Systemzeit mit NTP synchonisieren");
  Serial.println("=================================");
  if (MitInternetVerbinden())  // "Utility.h" ... In die weite Welt des Internet
  {
    configTzTime(TZ_INFO,NTP_SERVER); // ESP32 Systemzeit mit NTP Synchronisieren
    getLocalTime(&local,10000);       // Versuche 10 s zu Synchronisieren
    // "WiFi_Disconnect" ist in "Utility.h" zu finden
    if (getNTPtime(20)) {WiFi_Disconnect(); return true;} else {WiFi_Disconnect(); return false;}
  } else {WiFi_Disconnect(); return false;}
}

void ggfNeueServerZeit()
{
  // Wurde die aktuelle NTP-Zeit eingeholt wird die interne RTC entsprechend eingestellt. 
  // Konnte die NTP-Zeit nicht ermittelt werden, und die 
  // Zet des RTC ist gültig, wird die RTC-Zeit weiter verwendet. Nach einem PowerDown ist die RTC-Zeit auf jeden Fall
  // ungültig, und es muss eine NTP-Zeit geholt werden, in diesem Fall also Restart des Servers und neuer Versuch. 
  if (ServerZeitHolen()) 
  {
    Serial.println("NTP-Zeit wurde ermittelt!");
    setTime(NTP_now);
    Serial.print("Systemzeit nun: ");
    PrintDateTime(now());
    Serial.println(); Serial.println();
    #ifdef UseRtcDS1307
    DS1307.SetDateTime(now());
    #endif 
  }
  else
  {
    Serial.println("NTP-Zeit konnte nicht ermittelt werden! :-(");
    if (now()>1581600000) 
    {
      Serial.println("Alte SystemZeit wird weiter verwendet :-(");
      Serial.print("Systemzeit: ");
      PrintDateTime(now());
      Serial.println(); Serial.println();
    }
    else
    {
      #ifdef UseRtcDS1307
      if (DS1307.GetDateTime()>1581600000)
      {
        setTime(DS1307.GetDateTime());
        Serial.println("Als Systemzeit wird nun die RTC-DS1307-Zeit verwendet!");
        Serial.print("Systemzeit: ");
        PrintDateTime(now());
        Serial.println(); Serial.println();
      }
      else
      {
        Serial.println("RTC-DS1307-Zeit ist nicht korrekt und kann nicht verwendet werden! :-(");
        Serial.println("Neustart erfolgt in 5 Sekunden!");
        delay(5000);
        ESP.restart();
      }
      #else  
      Serial.println("Neustart erfolgt in 5 Sekunden!");
      delay(5000);
      ESP.restart();
      #endif 

    }
  }
}

