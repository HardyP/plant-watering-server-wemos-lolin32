void WebSiteErzeugenTabelle()
{
  strcat(TabStr,"<table>\n");
  strcat(TabStr,"<caption><a class=\"hrefsetup\" href=\"ClientSetup?ClientNr=");
  itoa(ServerTaskSelSens,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"\" title=\"Client-Einstellungen\">");
  strcat(TabStr,ServerTaskDaten[ServerTaskSelSens].tZeit);
  strcat(TabStr," >>> ");

  if(strlen(ServerTaskDaten[ServerTaskSelSens].Alias)==0) 
  {
    strcat(TabStr,ServerTaskDaten[ServerTaskSelSens].SensorKennung);
    strcat(TabStr,"</a>&nbsp;&nbsp;</caption>\n");
  }
  else 
  {
    strcat(TabStr,ServerTaskDaten[ServerTaskSelSens].Alias);
    strcat(TabStr," </a>&nbsp;&nbsp;</caption>\n");
  }
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<td>\n");
  strcat(TabStr,"<table>\n");
  strcat(TabStr,"<thead>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<td rowspan=\"2\"></td>\n");
  strcat(TabStr,"</thead>\n");
  strcat(TabStr,"<tbody>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">letztes Wasser</th>\n");
  strcat(TabStr,"<td>");
  strcat(TabStr,ServerTaskDaten[ServerTaskSelSens].tWasserZeit);
  strcat(TabStr,"</td>\n");
  strcat(TabStr," </tr>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">Feuchte</th>\n");
  strcat(TabStr,"<td>");
  itoa(ServerTaskDaten[ServerTaskSelSens].Feuchte,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr," </tr>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">minFeuchte</th>\n");
  strcat(TabStr,"<td>");
  itoa(ServerTaskDaten[ServerTaskSelSens].MinFeuchte,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"</tr>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">Temperatur</th>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].Temperatur,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"</tr>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">Spannung</th>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].Spannung,3,2,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"</tr>\n");
  strcat(TabStr,"</tbody>\n");
  strcat(TabStr,"</table>\n");
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>\n");
  strcat(TabStr,"<table>\n");
  strcat(TabStr,"<thead>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<td rowspan=\"2\"></td>\n");
  strcat(TabStr,"<th scope=\"col\" rowspan=\"2\">12h</th>\n");
  strcat(TabStr,"<th scope=\"col\" rowspan=\"2\">1d</th>\n");
  strcat(TabStr,"<th scope=\"col\" rowspan=\"2\">2d</th>\n");
  strcat(TabStr,"<th scope=\"col\" rowspan=\"2\">3d</th>\n");
  strcat(TabStr,"<th scope=\"col\" rowspan=\"2\">7d</th>\n");
  strcat(TabStr,"<th scope=\"col\" rowspan=\"2\">14d</th>\n");
  strcat(TabStr,"<th scope=\"col\" rowspan=\"2\">30d</th>\n");
  strcat(TabStr,"</thead>\n");
  strcat(TabStr,"<tbody>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">Wasser</th>\n");
  strcat(TabStr,"<td>");
  itoa(ServerTaskDaten[ServerTaskSelSens].Wasser12Stunden,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  itoa(ServerTaskDaten[ServerTaskSelSens].Wasser1Tage,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  itoa(ServerTaskDaten[ServerTaskSelSens].Wasser2Tage,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  itoa(ServerTaskDaten[ServerTaskSelSens].Wasser3Tage,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  itoa(ServerTaskDaten[ServerTaskSelSens].Wasser7Tage,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  itoa(ServerTaskDaten[ServerTaskSelSens].Wasser14Tage,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  itoa(ServerTaskDaten[ServerTaskSelSens].Wasser30Tage,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"</tr>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">+Temperatur</th>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMax12Stunden,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMax1Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMax2Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMax3Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMax7Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMax14Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMax30Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"</tr>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">-Temperatur</th>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMin12Stunden,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMin1Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMin2Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMin3Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMin7Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMin14Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturMin30Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"</tr>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">dTemperatur</th>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturD12Stunden,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturD1Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturD2Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturD3Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturD7Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturD14Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[ServerTaskSelSens].TemperaturD30Tage,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"</tr>\n");
  strcat(TabStr,"</tbody>\n");
  strcat(TabStr,"</table>\n");
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"</tr>\n");
  strcat(TabStr,"</table>\n");
  strcat(TabStr,"<p></p>\n");
  strcat(TabStr,"<!-- Ende Tabelle für einen Sensor -->\n");
 }

void WebSiteZurSD()
{
  if (sd.exists(WebSiteFileName)) 
  {
    if (!sd.remove(WebSiteFileName))  
    {
      Serial.println(" \r\nFilesystem _F_E_H_L_E_R_");
      Serial.println("========================");
      Serial.print("Datei: ");
      Serial.print(WebSiteFileName);
      Serial.println(" konnte nicht entfernt werden");
      Serial.println("Neustart erfolgt!");
      Serial.println();
      sd.initErrorHalt();
      delay(5000);
      ESP.restart();
    }
  }
  if (WebSiteFile.open(WebSiteFileName,FILE_WRITE)) 
  {
    Serial.print("Datei: ");
    Serial.print(WebSiteFileName);
    Serial.println(" wurde erzeugt");
  }
  else
  {
    Serial.println(" \r\nFilesystem _F_E_H_L_E_R_");
    Serial.println("========================");
    Serial.print("Datei: ");
    Serial.print(WebSiteFileName);
    Serial.println(" konnte nicht erzeugt werden");
    Serial.println("Neustart erfolgt!");
    Serial.println();
    sd.initErrorHalt();
    delay(5000);
    ESP.restart();
  }
  WebSiteFile.print(TabStr);
  WebSiteFile.close();
}

void WebSiteClientSetupZurSD()
{
  strcpy(WebSiteClientSetupFileName,"/ClientSetup");
  sprintf (LineBuffer,"%02lu",SetClientNr);
  strcat(WebSiteClientSetupFileName,LineBuffer);
  strcat(WebSiteClientSetupFileName,".html");
  if (sd.exists(WebSiteClientSetupFileName)) 
  {
    if (!sd.remove(WebSiteClientSetupFileName))  
    {
      Serial.println(" \r\nFilesystem _F_E_H_L_E_R_");
      Serial.println("========================");
      Serial.print("Datei: ");
      Serial.print(WebSiteClientSetupFileName);
      Serial.println(" konnte nicht entfernt werden");
      Serial.println("Neustart erfolgt!");
      Serial.println();
      sd.initErrorHalt();
      delay(5000);
      ESP.restart();
    }
  }
  if (WebSiteClientSetupFile.open(WebSiteClientSetupFileName,FILE_WRITE)) 
  {
    Serial.print("Datei: ");
    Serial.print(WebSiteClientSetupFileName);
    Serial.println(" wurde erzeugt");
  }
  else
  {
    Serial.println(" \r\nFilesystem _F_E_H_L_E_R_");
    Serial.println("========================");
    Serial.print("Datei: ");
    Serial.print(WebSiteClientSetupFileName);
    Serial.println(" konnte nicht erzeugt werden");
    Serial.println("Neustart erfolgt!");
    Serial.println();
    sd.initErrorHalt();
    delay(5000);
    ESP.restart();
  }
  WebSiteClientSetupFile.print(TabStr);
  WebSiteClientSetupFile.close();
}

void WebSiteHeadErzeugen()
{
  strcpy(HeadStr,"");
  if (!VorlageFile.open(VorlageFileName,FILE_READ)) {Serial.println(" VorlageFile konnte nicht erzeugt werden!");}
  VorlageFile.open(VorlageFileName,FILE_READ);
  while (VorlageFile.available()&&(LineString.indexOf("<!--")==-1)) 
  {
    VorlageFile.fgets(LineBuffer,sizeof(LineBuffer));
    LineString=LineBuffer;
    strcat(HeadStr,LineBuffer);
  }
  VorlageFile.close();
  int belegt=strlen(HeadStr);
  int verfuegbar=sizeof(HeadStr)-belegt;
  Serial.print("Head der WebSite erzeugt: ");
  Serial.print(belegt);
  Serial.print("Byte belegt, ");
  Serial.print(verfuegbar);
  Serial.println("Byte noch verfügbar");
}

void WebSiteConfigErzeugen()
{
  strcpy(TabStr,HeadStr);
  strcat(TabStr,"<table>\n");
  strcat(TabStr,"<caption> ");
  strcat(TabStr,ServerTaskDaten[SetClientNr].SensorKennung);
  if(strlen(ServerTaskDaten[SetClientNr].Alias)==0) {strcat(TabStr," &nbsp;&nbsp;</caption>\n");}
  else 
  {
    strcat(TabStr," (");
    strcat(TabStr,ServerTaskDaten[SetClientNr].Alias);
    strcat(TabStr,") &nbsp;&nbsp;</caption>\n");
  }
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<td>\n");
  strcat(TabStr,"<table>\n");
  strcat(TabStr,"<thead>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<td rowspan=\"2\"></td>\n");
  strcat(TabStr,"</thead>\n");
  strcat(TabStr,"<tbody>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">letztes Wasser</th>\n");
  strcat(TabStr,"<td>");
  strcat(TabStr,ServerTaskDaten[SetClientNr].tWasserZeit);
  strcat(TabStr,"</td>\n");
  strcat(TabStr," </tr>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">Feuchte</th>\n");
  strcat(TabStr,"<td>");
  itoa(ServerTaskDaten[SetClientNr].Feuchte,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr," </tr>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">minFeuchte</th>\n");
  strcat(TabStr,"<td>");
  itoa(ServerTaskDaten[SetClientNr].MinFeuchte,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"</tr>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">Temperatur</th>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[SetClientNr].Temperatur,3,1,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"</tr>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<th scope=\"row\">Spannung</th>\n");
  strcat(TabStr,"<td>");
  dtostrf(ServerTaskDaten[SetClientNr].Spannung,3,2,buffer);
  strcat(TabStr,buffer);
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"</tr>\n");
  strcat(TabStr,"</tbody>\n");
  strcat(TabStr,"</table>\n");
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"<td>\n");
  strcat(TabStr,"<table>\n");
  strcat(TabStr,"<form action=\"/\">\n");
  strcat(TabStr,"<thead>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<td ></td>\n");
  strcat(TabStr,"<th scope=\"col\" colspan=\"2\">Client-Einstellungen</th>\n");
  strcat(TabStr,"</tr>\n");
  strcat(TabStr,"</thead>\n");
  strcat(TabStr,"<tbody>\n");
  strcat(TabStr,"<tr>\n");
  strcat(TabStr,"<td ></td>\n");
  strcat(TabStr,"<td colspan=\"2\"><label for=\"alias\">ClientName: <input id=\"alias\" type=\"text\" name=\"alias\" size=\"30\" maxlength=\"30\"  value=\"");
  strcat(TabStr,ServerTaskDaten[SetClientNr].Alias);
  strcat(TabStr,"\"></label> <br />\n");
  strcat(TabStr,"<label for=\"c_pause\">Client alle <input id=\"c_pause\" type=\"number\" step=\"1\" min=\"5\" max=\"60\" value=\"");
  itoa(ServerTaskDaten[SetClientNr].SchlafZeit/60,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"\" name=\"c_pause\" > Minuten aufwachen</label> <br />\n");
  strcat(TabStr,"<label for=\"c_pump\">Pumpzeit für Wasser <input id=\"c_pump\" type=\"number\" step=\"1\" min=\"5\" max=\"50\" value=\"");
  itoa(ServerTaskDaten[SetClientNr].PumpZeit,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"\" name=\"c_pump\" > Sekunden</label>  <br />\n");
  strcat(TabStr,"<label for=\"c_wasser\">Wasser maximal alle <input id=\"c_wasser\" type=\"number\" step=\"1\" min=\"10\" max=\"1440\" value=\"");
  itoa(ServerTaskDaten[SetClientNr].WartenBisWasserS/60,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"\" name=\"c_wasser\" > Minuten</label>  <br />\n");
  strcat(TabStr,"<label for=\"c_feuchte\">Mindest-Feuchte <input id=\"c_feuchte\" type=\"number\" step=\"1\" min=\"80\" max=\"900\" value=\"");
  itoa(ServerTaskDaten[SetClientNr].MinFeuchte,buffer,10);
  strcat(TabStr,buffer);
  strcat(TabStr,"\" name=\"c_feuchte\" > </label>  <br />\n");
  strcat(TabStr,"<td ><button type=\"submit\" name=\"action\" value=\"0\">Abbruch</button>  <br />\n");
  strcat(TabStr,"<button type=\"submit\" name=\"action\" value=\"1\">OK</button></td>\n");
  strcat(TabStr,"</tr>\n");
  strcat(TabStr,"</tbody>\n");
  strcat(TabStr,"</form>\n");
  strcat(TabStr,"</table>\n");
  strcat(TabStr,"</td>\n");
  strcat(TabStr,"</table>\n");
  strcat(TabStr,"<p></p>\n");
  strcat(TabStr,"</div>\n");
  strcat(TabStr,"</div>\n");
  strcat(TabStr,"</body>\n");
  strcat(TabStr,"</html>\n");
  int belegt=strlen(TabStr);
  int verfuegbar=sizeof(TabStr)-belegt;
  Serial.print("Config-WebSite erzeugt: ");
  Serial.print(belegt);
  Serial.print("Byte belegt, ");
  Serial.print(verfuegbar);
  Serial.println("Byte noch verfügbar");
  WebSiteClientSetupZurSD();
}

void WebSiteErzeugen()
{
  strcpy(TabStr,HeadStr);
  for (ServerTaskSelSens=0;ServerTaskSelSens<ServerTaskAnzSens;ServerTaskSelSens++) {WebSiteErzeugenTabelle();}
  strcat(TabStr,"</div>\n");
  strcat(TabStr,"</div>\n");
  strcat(TabStr,"</body>\n");
  strcat(TabStr,"</html>\n");
  int belegt=strlen(TabStr);
  int verfuegbar=sizeof(TabStr)-belegt;
  Serial.print("WebSite erzeugt: ");
  Serial.print(belegt);
  Serial.print("Byte belegt, ");
  Serial.print(verfuegbar);
  Serial.println("Byte noch verfügbar");
  WebSiteZurSD();
}
