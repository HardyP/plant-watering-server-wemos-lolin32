void ServerTaskDatenAnalyse()
{
  // Die Clientendaten der letzten 30Tage werden aufbereitet 
  char Kennung[20];
  time_t Stunden12;
  time_t Tage1;
  time_t Tage2;
  time_t Tage3;
  time_t Tage7;
  time_t Tage14;
  time_t Tage30;
  time_t WasserZeit=0;
  int WasserZeitM=0;
  int WasserZeitH=0;
  int WasserZeitD=0;
  Stunden12=now()-Sekunden12h;
  Tage1=now()-Sekunden1d;
  Tage2=now()-Sekunden2d;
  Tage3=now()-Sekunden3d;
  Tage7=now()-Sekunden7d;
  Tage14=now()-Sekunden14d;
  Tage30=now()-Sekunden30d;
  ServerTaskDaten[ServerTaskSelSens].Wasser12Stunden=0;
  ServerTaskDaten[ServerTaskSelSens].Wasser1Tage=0;
  ServerTaskDaten[ServerTaskSelSens].Wasser2Tage=0;
  ServerTaskDaten[ServerTaskSelSens].Wasser3Tage=0;
  ServerTaskDaten[ServerTaskSelSens].Wasser7Tage=0;
  ServerTaskDaten[ServerTaskSelSens].Wasser14Tage=0;
  ServerTaskDaten[ServerTaskSelSens].Wasser30Tage=0;
  ServerTaskDaten[ServerTaskSelSens].WasserZeit=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMax12Stunden=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMax1Tage=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMax2Tage=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMax3Tage=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMax7Tage=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMax14Tage=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMax30Tage=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMin12Stunden=100;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMin1Tage=100;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMin2Tage=100;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMin3Tage=100;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMin7Tage=100;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMin14Tage=100;
  ServerTaskDaten[ServerTaskSelSens].TemperaturMin30Tage=100;
  ServerTaskDaten[ServerTaskSelSens].TemperaturD12Stunden=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturD1Tage=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturD2Tage=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturD3Tage=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturD7Tage=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturD14Tage=0;
  ServerTaskDaten[ServerTaskSelSens].TemperaturD30Tage=0;
  TemperaturD12StundenCount=0;
  TemperaturD1TageCount=0;
  TemperaturD2TageCount=0;
  TemperaturD3TageCount=0;
  TemperaturD7TageCount=0;
  TemperaturD14TageCount=0;
  TemperaturD30TageCount=0;
  ChirpFile.open(ChirpFileName,FILE_READ);
  while (ChirpFile.available()) 
  {
    ChirpFile.fgets(LineBuffer,sizeof(LineBuffer));
    LineString=LineBuffer;
    ServerTaskZeilenAnalyseSD();
    SensorKennung.toCharArray(Kennung,20);
    if (strcmp(Kennung,ServerTaskDaten[ServerTaskSelSens].SensorKennung)==0)       
    {
      if (Wasser>0) {WasserZeit=now()-Zeit;}
      if (Zeit>=Stunden12) 
      {
        ServerTaskDaten[ServerTaskSelSens].Wasser12Stunden+=Wasser;
        if (Temperatur>ServerTaskDaten[ServerTaskSelSens].TemperaturMax12Stunden) {ServerTaskDaten[ServerTaskSelSens].TemperaturMax12Stunden=Temperatur;}
        if (Temperatur<ServerTaskDaten[ServerTaskSelSens].TemperaturMin12Stunden) {ServerTaskDaten[ServerTaskSelSens].TemperaturMin12Stunden=Temperatur;}
        TemperaturD12StundenCount++;
        ServerTaskDaten[ServerTaskSelSens].TemperaturD12Stunden+=Temperatur;
      }
      if (Zeit>=Tage1) 
      {
        ServerTaskDaten[ServerTaskSelSens].Wasser1Tage+=Wasser;
        if (Temperatur>ServerTaskDaten[ServerTaskSelSens].TemperaturMax1Tage) {ServerTaskDaten[ServerTaskSelSens].TemperaturMax1Tage=Temperatur;}
        if (Temperatur<ServerTaskDaten[ServerTaskSelSens].TemperaturMin1Tage) {ServerTaskDaten[ServerTaskSelSens].TemperaturMin1Tage=Temperatur;}
        TemperaturD1TageCount++;
        ServerTaskDaten[ServerTaskSelSens].TemperaturD1Tage+=Temperatur;
      }
      if (Zeit>=Tage2) 
      {
        ServerTaskDaten[ServerTaskSelSens].Wasser2Tage+=Wasser;
        if (Temperatur>ServerTaskDaten[ServerTaskSelSens].TemperaturMax2Tage) {ServerTaskDaten[ServerTaskSelSens].TemperaturMax2Tage=Temperatur;}
        if (Temperatur<ServerTaskDaten[ServerTaskSelSens].TemperaturMin2Tage) {ServerTaskDaten[ServerTaskSelSens].TemperaturMin2Tage=Temperatur;}
        TemperaturD2TageCount++;
        ServerTaskDaten[ServerTaskSelSens].TemperaturD2Tage+=Temperatur;
      }
      if (Zeit>=Tage3) 
      {
        ServerTaskDaten[ServerTaskSelSens].Wasser3Tage+=Wasser;
        if (Temperatur>ServerTaskDaten[ServerTaskSelSens].TemperaturMax3Tage) {ServerTaskDaten[ServerTaskSelSens].TemperaturMax3Tage=Temperatur;}
        if (Temperatur<ServerTaskDaten[ServerTaskSelSens].TemperaturMin3Tage) {ServerTaskDaten[ServerTaskSelSens].TemperaturMin3Tage=Temperatur;}
        TemperaturD3TageCount++;
        ServerTaskDaten[ServerTaskSelSens].TemperaturD3Tage+=Temperatur;
      }
      if (Zeit>=Tage7) 
      {
        ServerTaskDaten[ServerTaskSelSens].Wasser7Tage+=Wasser;
        if (Temperatur>ServerTaskDaten[ServerTaskSelSens].TemperaturMax7Tage) {ServerTaskDaten[ServerTaskSelSens].TemperaturMax7Tage=Temperatur;}
        if (Temperatur<ServerTaskDaten[ServerTaskSelSens].TemperaturMin7Tage) {ServerTaskDaten[ServerTaskSelSens].TemperaturMin7Tage=Temperatur;}
        TemperaturD7TageCount++;
        ServerTaskDaten[ServerTaskSelSens].TemperaturD7Tage+=Temperatur;
      }
      if (Zeit>=Tage14) 
      {
        ServerTaskDaten[ServerTaskSelSens].Wasser14Tage+=Wasser;
        if (Temperatur>ServerTaskDaten[ServerTaskSelSens].TemperaturMax14Tage) {ServerTaskDaten[ServerTaskSelSens].TemperaturMax14Tage=Temperatur;}
        if (Temperatur<ServerTaskDaten[ServerTaskSelSens].TemperaturMin14Tage) {ServerTaskDaten[ServerTaskSelSens].TemperaturMin14Tage=Temperatur;}
        TemperaturD14TageCount++;
        ServerTaskDaten[ServerTaskSelSens].TemperaturD14Tage+=Temperatur;
      }
      if (Zeit>=Tage30) 
      {
        ServerTaskDaten[ServerTaskSelSens].Wasser30Tage+=Wasser;
        if (Temperatur>ServerTaskDaten[ServerTaskSelSens].TemperaturMax30Tage) {ServerTaskDaten[ServerTaskSelSens].TemperaturMax30Tage=Temperatur;}
        if (Temperatur<ServerTaskDaten[ServerTaskSelSens].TemperaturMin30Tage) {ServerTaskDaten[ServerTaskSelSens].TemperaturMin30Tage=Temperatur;}
        TemperaturD30TageCount++;
        ServerTaskDaten[ServerTaskSelSens].TemperaturD30Tage+=Temperatur;
      }
    }
  } 
  ChirpFile.close();
  WasserZeitM=WasserZeit/60;
  WasserZeitH=WasserZeitM/60;
  WasserZeitD=WasserZeitH/24;
  WasserZeitM=WasserZeitM-WasserZeitH*60;
  WasserZeitH=WasserZeitH-WasserZeitD*24;
  ServerTaskDaten[ServerTaskSelSens].WasserZeit=WasserZeit;
  sprintf(LineBuffer,"%02d",WasserZeitD); 
  strcpy(ServerTaskDaten[ServerTaskSelSens].tWasserZeit,LineBuffer);
  strcat(ServerTaskDaten[ServerTaskSelSens].tWasserZeit,":");
  sprintf(LineBuffer,"%02d",WasserZeitH); 
  strcat(ServerTaskDaten[ServerTaskSelSens].tWasserZeit,LineBuffer);
  strcat(ServerTaskDaten[ServerTaskSelSens].tWasserZeit,":");
  sprintf(LineBuffer,"%02d",WasserZeitM); 
  strcat(ServerTaskDaten[ServerTaskSelSens].tWasserZeit,LineBuffer);
  ServerTaskDaten[ServerTaskSelSens].TemperaturD12Stunden/=TemperaturD12StundenCount;
  ServerTaskDaten[ServerTaskSelSens].TemperaturD1Tage/=TemperaturD1TageCount;
  ServerTaskDaten[ServerTaskSelSens].TemperaturD2Tage/=TemperaturD2TageCount;
  ServerTaskDaten[ServerTaskSelSens].TemperaturD3Tage/=TemperaturD3TageCount;
  ServerTaskDaten[ServerTaskSelSens].TemperaturD7Tage/=TemperaturD7TageCount;
  ServerTaskDaten[ServerTaskSelSens].TemperaturD14Tage/=TemperaturD14TageCount;
  ServerTaskDaten[ServerTaskSelSens].TemperaturD30Tage/=TemperaturD30TageCount;
  Serial.println(); 
  Serial.print("  Client: ");           
  Serial.println(ServerTaskDaten[ServerTaskSelSens].SensorKennung);
  if (strlen(ServerTaskDaten[ServerTaskSelSens].Alias)>0)
  {
    Serial.print("  ---> ");           
    Serial.print(ServerTaskDaten[ServerTaskSelSens].Alias);           
    Serial.println(" <---"); 
  }
  Serial.print("  >> "); 
  strftime(buffer,20,"%d.%m.%Y-%H:%M",localtime(&Zeit));
  Serial.print(buffer); 
  Serial.println("  << "); 

  Serial.println(".----------------------------."); 
  Serial.printf("! %16s","letztes Wasser:");           
  Serial.printf("%10s !",ServerTaskDaten[ServerTaskSelSens].tWasserZeit);           
  Serial.println(); 
  Serial.printf("! %16s","Feuchte:");           
  Serial.printf("%10d !",ServerTaskDaten[ServerTaskSelSens].Feuchte);           
  Serial.println(); 
  Serial.printf("! %16s","minFeuchte:");           
  Serial.printf("%10d !",ServerTaskDaten[ServerTaskSelSens].MinFeuchte);           
  Serial.println(); 
  Serial.printf("! %16s","Temperatur:");           
  Serial.printf("%10.1f !",ServerTaskDaten[ServerTaskSelSens].Temperatur);           
  Serial.println(); 
  Serial.printf("! %16s","Spannung:");           
  Serial.printf("%10.2f !",ServerTaskDaten[ServerTaskSelSens].Spannung);           
  Serial.println(); 
  Serial.println(" ---------------------------- "); 
  Serial.println(); 
  Serial.printf("%13s","");           
  Serial.printf(" ! %6s","12h");           
  Serial.printf(" ! %6s","1d");           
  Serial.printf(" ! %6s","2d");           
  Serial.printf(" ! %6s","3d");           
  Serial.printf(" ! %6s","7d");           
  Serial.printf(" ! %6s","14d");           
  Serial.printf(" ! %6s !","30d");           
  Serial.println(); 
  Serial.println(".-------------!--------!--------!--------!--------!--------!--------!--------!"); 
  Serial.printf("!%12s","Wasser");           
  Serial.printf(" ! %6d",ServerTaskDaten[ServerTaskSelSens].Wasser12Stunden);           
  Serial.printf(" ! %6d",ServerTaskDaten[ServerTaskSelSens].Wasser1Tage);           
  Serial.printf(" ! %6d",ServerTaskDaten[ServerTaskSelSens].Wasser2Tage);           
  Serial.printf(" ! %6d",ServerTaskDaten[ServerTaskSelSens].Wasser3Tage);           
  Serial.printf(" ! %6d",ServerTaskDaten[ServerTaskSelSens].Wasser7Tage);           
  Serial.printf(" ! %6d",ServerTaskDaten[ServerTaskSelSens].Wasser14Tage);           
  Serial.printf(" ! %6d !",ServerTaskDaten[ServerTaskSelSens].Wasser30Tage);           
  Serial.println(); 
  Serial.printf("!%12s","+Temperatur");           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturMax12Stunden);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturMax1Tage);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturMax2Tage);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturMax3Tage);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturMax7Tage);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturMax14Tage);           
  Serial.printf(" ! %6.1f !",ServerTaskDaten[ServerTaskSelSens].TemperaturMax30Tage);           
  Serial.println(); 
  Serial.printf("!%12s","-Temperatur");           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturMin12Stunden);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturMin1Tage);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturMin2Tage);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturMin3Tage);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturMin7Tage);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturMin14Tage);           
  Serial.printf(" ! %6.1f !",ServerTaskDaten[ServerTaskSelSens].TemperaturMin30Tage);           
  Serial.println(); 
  Serial.printf("!%12s","dTemperatur");           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturD12Stunden);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturD1Tage);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturD2Tage);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturD3Tage);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturD7Tage);           
  Serial.printf(" ! %6.1f",ServerTaskDaten[ServerTaskSelSens].TemperaturD14Tage);           
  Serial.printf(" ! %6.1f !",ServerTaskDaten[ServerTaskSelSens].TemperaturD30Tage);           
  Serial.println(); 
  Serial.println(" -------------!--------!--------!--------!--------!--------!--------!-------- "); 
  Serial.println(); 
}
