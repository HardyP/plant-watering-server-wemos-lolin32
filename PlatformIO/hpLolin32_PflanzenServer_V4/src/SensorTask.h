bool ggfSetSensor()
{
  // Prüfung ob der ServerTask bereits Setup-Daten des aktuellen Clienten gesendet hat
  bool Gefunden=false;
  ST_sIncomingPacket=String(incomingPacket);
  int Colon1=ST_sIncomingPacket.indexOf(';');
  ST_sSensorKennung=ST_sIncomingPacket.substring(0,Colon1);
  ST_sSensorKennung.toCharArray(ST_SensorKennung,20);
  if (ST_AnzSens>0)
  {
    ST_SelSens=-1;
    while ((!Gefunden)&&(ST_AnzSens!=ST_SelSens+1)) 
    {
      ST_SelSens++;
      if (strcmp(ST_SensorKennung,SensorTaskDaten[ST_SelSens].SensorKennung)==0) {Gefunden=true;}
    }
    if (Gefunden) 
    {
      strcpy(ST_SensorKennung,SensorTaskDaten[ST_SelSens].SensorKennung);
      strcpy(ST_SensorSetup,SensorTaskDaten[ST_SelSens].SensorSetup);
    }
  }
  return Gefunden;
}

void SensorSetupSpeichern()
{
  // Vom ServerTask empfangene Setup-Daten des aktuellen Clienten werden hier abgelegt
  bool Gefunden=false;
  ST_sPacketFromServerTask=String(PacketFromServerTask);
  int Colon1=ST_sPacketFromServerTask.indexOf(';');
  ST_sSensorKennung=ST_sPacketFromServerTask.substring(0,Colon1);
  ST_sSensorSetup=ST_sPacketFromServerTask.substring(Colon1+1,PacketSizeMax);
  ST_sSensorKennung.toCharArray(ST_SensorKennung,20);
  ST_sSensorSetup.toCharArray(ST_SensorSetup,PacketSizeMax);
  if (ST_AnzSens>0)
  {
    ST_SelSens=-1;
    while ((!Gefunden)&&(ST_AnzSens!=ST_SelSens+1)) 
    {
      ST_SelSens++;
      if (strcmp(ST_SensorKennung,SensorTaskDaten[ST_SelSens].SensorKennung)==0) {Gefunden=true;}
    }
    if (!Gefunden) 
    {
      ST_AnzSens++;
      ST_SelSens=ST_AnzSens-1;
    }
  }
  else
  {
    ST_AnzSens=1;
    ST_SelSens=0;
  }
  strcpy(SensorTaskDaten[ST_SelSens].SensorKennung,ST_SensorKennung);
  strcpy(SensorTaskDaten[ST_SelSens].SensorSetup,ST_SensorSetup);
}

void UdpEmpfangBestaetigen()
{
  // Wenn der Client dem Server bekannt ist, erhält er hier eine Empfangsquittung.
  // Dieses erfolgt in Form der gespeicherten Setup-Daten. Sollte der Client dem vorherigen Client
  // zu nahe gekommen sein, bekommt er eine zusätzliche Schlafzeit. Einstelldaten dazu in "setup_daten.h" 
  if (ST_AnzTimeLastUDP>0) 
  {
    ST_AnzTimeLastUDP--;
    if (SerialDEBUG) {Serial.print("AnzTimeLastUDP: "); Serial.println(ST_AnzTimeLastUDP);}
  } 
  if ((ST_DiffTimeLastUDP<=MinDiffTimeUDP)&&(ST_AnzTimeLastUDP<1))
  {
    ST_AnzTimeLastUDP=ST_AnzSens;
    ST_ExtraSchlafZeit=AddDiffTimeUDP;
    if (SerialDEBUG) {Serial.print(ST_ExtraSchlafZeit); Serial.println(" Sekunden Extra-Schlafzeit!");}
  }
  else {ST_ExtraSchlafZeit=0;}
  Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
  Udp.print(":-)");
  Udp.print(";");
  Udp.print(ST_SensorSetup);
  Udp.print(ST_ExtraSchlafZeit);
  Udp.print(";");
  Udp.endPacket();
  Serial.printf("Empfangsbestätigung an %s versendet: ",Udp.remoteIP().toString().c_str());
  Serial.print(":-)"); Serial.print(";"); Serial.print(ST_SensorSetup); Serial.print(ST_ExtraSchlafZeit); Serial.println(";");
}

void LoopSensorTask(void * parameter) 
{
  //  Datenaustausch zwischen Server und den einzelnen Clienten, dazu wird UDP verwendet.
  //  UDP wurde ganz gezielt ausgewählt, da für den Server ein günstiger ESP verwendet werden sollte. Da sich Core0 ausschließlich 
  //  um diesen Datenverkehr kümmert, sind seine Reaktionen sehr schnell, und die Clienten nur kurz mit dem Server verbunden. Zusätzlich 
  //  wird versucht, durch kleine Veränderung der Schlafzeit der Clienten, ein gleichzeitiges Erwachen zu verhindern. 
  //  Die einzelnen Clienten schicken ein Datenpaket an den Server mit den Messwerten. Der Server sendet als Antwort eine Empfangsquittung 
  //  an den betreffenden Clienten zurück, ggf mit neuen Setup-Daten (zB. Schlafzeit etc.). Sollte ein Client keine Empfangsquittung erhalten, 
  //  arbeitet er ohne Behinderung weiter.
  for (;;) 
  {
    int packetSize=Udp.parsePacket();
    if (packetSize)
    {
      BlinkSensorLed(); // "Utility.h" ... Blinke Core0_LED ein mal
      Serial.printf("UDP-Paket mit %d bytes von %s, port %d empfangen!\n",packetSize,Udp.remoteIP().toString().c_str(),Udp.remotePort());
      int len=Udp.read(incomingPacket,PacketSizeMax);
      if (len>0) {incomingPacket[len]=0;}
      Serial.printf("UDP-Paketinhalt: %s\n",incomingPacket);
      ST_DiffTimeLastUDP=now()-ST_TimeLastUDP;
      ST_TimeLastUDP=now();
      if (SerialDEBUG) {Serial.print("DiffTimeLastUDP: "); Serial.println(ST_DiffTimeLastUDP);}
      if (ggfSetSensor()) // Prüfen ob der Client dem Server bekannt ist
      {
        if (SerialDEBUG) 
        {
          Serial.println("Daten für Sensor-Setup!");
          Serial.print("AnzSens: "); Serial.println(ST_AnzSens);
          Serial.print("SelSens: "); Serial.println(ST_SelSens);
          Serial.print("SensorKennung: "); Serial.println(ST_sSensorKennung);
          Serial.print("SensorSetup: "); Serial.println(ST_sSensorSetup);
        }
        UdpEmpfangBestaetigen(); // Der SensorTask hat bereits SetUp-Daten vom ServerTask erhalten, SetUp-Daten werden gesendet
      }
      else {Serial.println("Keine Daten für Sensor-Setup verfügbar!");}
      xQueueSend(QueueToServerTask,incomingPacket,portMAX_DELAY); // UDP-Datenpaket wird in die Queue für den ServerTask gelegt
      if (SerialDEBUG) {Serial.print("PacketToServerTask: "); Serial.println(incomingPacket);}
    }
    if( xQueueReceive(QueueToSensorTask,PacketFromServerTask,(TickType_t)10)) // Prüfung ob Daten in der Queue vom ServerTask vorliegen
    {
      if (SerialDEBUG) {Serial.print("PacketFromServerTask: "); Serial.println(PacketFromServerTask);}
      SensorSetupSpeichern(); // Empangene Setup-Daten vom ServerTask werden abgelegt
      if (SerialDEBUG) 
      {
      Serial.println("Daten für Sensor-Setup gespeichert!");
      Serial.print("AnzSens: "); Serial.println(ST_AnzSens);
      Serial.print("SelSens: "); Serial.println(ST_SelSens);
      Serial.print("SensorKennung: "); Serial.println(ST_sSensorKennung);
      Serial.print("SensorSetup: "); Serial.println(ST_sSensorSetup);
      }
    }
	}
}



