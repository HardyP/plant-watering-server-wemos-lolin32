void ServerTask_to_SensorTask(int SelSens)
{
  // Daten des aktuellen Clienten in die Queue zum den SensorTask ablegen
  strcpy(PacketToSensorTask,ServerTaskDaten[SelSens].SensorKennung);
  strcat(PacketToSensorTask,";");
  itoa(ServerTaskDaten[SelSens].MinFeuchte,buffer,10);
  strcat(PacketToSensorTask,buffer);
  strcat(PacketToSensorTask,";");
  itoa(ServerTaskDaten[SelSens].SchlafZeit,buffer,10);
  strcat(PacketToSensorTask,buffer);
  strcat(PacketToSensorTask,";");
  itoa(ServerTaskDaten[SelSens].WartenBisWasserS,buffer,10);
  strcat(PacketToSensorTask,buffer);
  strcat(PacketToSensorTask,";");
  itoa(ServerTaskDaten[SelSens].PumpZeit,buffer,10);
  strcat(PacketToSensorTask,buffer);
  strcat(PacketToSensorTask,";");
  strcat(PacketToSensorTask,ServerTaskDaten[SelSens].Alias);
  strcat(PacketToSensorTask,";");
  xQueueSend(QueueToSensorTask,PacketToSensorTask,portMAX_DELAY);
  if (SerialDEBUG) {Serial.print("PacketToSensorTask: ");	Serial.println(PacketToSensorTask);}
}

void handleGenericArgs()
{
  String message = "Anzahl empfangene Argumente: ";
  message += server.args();
  message += "\n";
  for (int i=0; i<server.args(); i++)
  {
    message+="ArgumentNr "+(String)i+" –> ";
    message+=server.argName(i)+": ";
    message+=server.arg(i)+"\n";
  } 
  Serial.print(message);
}
 
void handleIndex()
{
  // Handle für "/index.html"
  if (SetClientNr<MaxClients+1) 
  {
    handleGenericArgs();
    if (server.arg("action")=="1")
    {
      Serial.println("OK gedrückt");
      server.arg("alias").toCharArray(ServerTaskDaten[SetClientNr].Alias,31);
      ServerTaskDaten[SetClientNr].SchlafZeit=server.arg("c_pause").toInt()*60; 
      ServerTaskDaten[SetClientNr].WartenBisWasserS=server.arg("c_wasser").toInt()*60; 
      ServerTaskDaten[SetClientNr].PumpZeit=server.arg("c_pump").toInt(); 
      ServerTaskDaten[SetClientNr].MinFeuchte=server.arg("c_feuchte").toInt(); 
      ServerTask_to_SensorTask(SetClientNr);
    }
    else {Serial.println("Abbruch gedrückt");}
    WebSiteErzeugen(); // "WebSiteErzeugen.h" ... Es wird die "/index.html" auf der SD-Karte erzeugt.
    Serial.println(" ---------------------------------");
  }
  SetClientNr=MaxClients+1;
  StreamPath=WebSiteFileName;
  StreamDataType="text/html";
  Serial.print("handleIndex-StreamPath --> ");
  Serial.println(StreamPath);
  if (StreamFile.open(StreamPath.c_str()))
  {  
    while (StreamFile.available()) 
    {
      StreamBufLen=StreamFile.read(StreamBuf,sizeof(StreamBuf));
      server.client().write((const char*)StreamBuf,StreamBufLen);
    }
    StreamFile.close();
  }
  else
  {
    String message = "F E H L E R ! Konnte 'index.html' nicht oeffnen!\n\n";
    server.send(404, "text/plain", message);
    Serial.print(message);
  }
}

void handleClientSetup()
{
  // Handle zur Konfiguration der einzelnen Clienten
  Serial.println(" -----> handleClientSetup <-----");
  handleGenericArgs();
  SetClientNr=server.arg(0).toInt();
  Serial.print("Bearbeitung ClientNr.: ");
  Serial.println(SetClientNr);
  WebSiteConfigErzeugen();// "WebSiteErzeugen.h" ... Es wird eine WebSite zur Eingabe der Setup-Daten des Clienten erzeugt
  StreamPath=WebSiteClientSetupFileName;
  StreamDataType="text/html";
  Serial.print("handleClientSetup-StreamPath --> ");
  Serial.println(StreamPath);
  if (StreamFile.open(StreamPath.c_str()))
  {  
    while (StreamFile.available()) 
    {
      StreamBufLen=StreamFile.read(StreamBuf,sizeof(StreamBuf));
      server.client().write((const char*)StreamBuf,StreamBufLen);
    }
    StreamFile.close();
  }
  else
  {
    String message = "F E H L E R ! Konnte 'ClientSetupXX' nicht oeffnen!\n\n";
    server.send(404, "text/plain", message);
    Serial.print(message);
  }
}

void handleNotFound()
{
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++)
    {message +=" "+ server.argName(i)+": "+server.arg(i)+"\n";}
  server.send(404, "text/plain", message);
  Serial.print(message);
}
 
void ServerTaskSelSensErmitteln()
{
  // Falls bereits Daten des aktuellen Clienten vorhanden sind, wird der Zeiger "ServerTaskAnzSens" dazu ermittelt.
  // Ist der aktuelle Client noch unbekannt, wird ein Datensatz angelegt
  bool Gefunden=false;
  char Kennung[20];
  SensorKennung.toCharArray(Kennung,20);
  if (ServerTaskAnzSens>0)
  {
    ServerTaskSelSens=-1;
    while ((!Gefunden)&&(ServerTaskAnzSens!=ServerTaskSelSens+1)) 
    {
      ServerTaskSelSens++;
      if (strcmp(Kennung,ServerTaskDaten[ServerTaskSelSens].SensorKennung)==0) {Gefunden=true;}
    }
    if (!Gefunden) 
    {
      ServerTaskAnzSens++;
      ServerTaskSelSens=ServerTaskAnzSens-1;
    }
  }
  else
  {
    ServerTaskAnzSens=1;
    ServerTaskSelSens=0;
  }
  if (!Gefunden) 
  {
    SensorKennung.toCharArray(ServerTaskDaten[ServerTaskSelSens].SensorKennung,20);
    Alias.toCharArray(ServerTaskDaten[ServerTaskSelSens].Alias,31);
    ServerTaskDaten[ServerTaskSelSens].MinFeuchte=MinFeuchte;
    ServerTaskDaten[ServerTaskSelSens].SchlafZeit=SchlafZeit;
    ServerTaskDaten[ServerTaskSelSens].WartenBisWasserS=WartenBisWasserS;
    ServerTaskDaten[ServerTaskSelSens].PumpZeit=PumpZeit; 
  }
  ServerTaskDaten[ServerTaskSelSens].Zeit=Zeit;
  strftime(ServerTaskDaten[ServerTaskSelSens].tZeit,18,"%d.%m.%Y-%H:%M",localtime(&Zeit));
  ServerTaskDaten[ServerTaskSelSens].Feuchte=Feuchte;
  ServerTaskDaten[ServerTaskSelSens].Wasser=Wasser;
  ServerTaskDaten[ServerTaskSelSens].Spannung=Spannung;
  ServerTaskDaten[ServerTaskSelSens].Temperatur=Temperatur;
  ServerTaskDaten[ServerTaskSelSens].WartenBisWasser=WartenBisWasser;
}

void LoopServerTask(void * parameter) 
{
  //  ServerTask ist für die Aufbereitung der Daten, Erzeugung der WebSite und den lokalen Server zuständig. Er arbeitet vollkommen asynchron zum
  //  Datenverkehr des SensorTask mit den Clienten.
  //  SensorTask (Core0) und ServerTask (Core1) sind über zwei Queue verbunden.
  //  QueueToServerTask: Datenverkehr von SensorTask zu ServerTask. Über diese Queue sendet der SensorTask die von 
  //  Clienten empfangenen Daten an den ServerTask zur weiteren Verarbeitung.
  //  QueueToSensorTask: Datenverkehr von ServerTask zu SensorTask. Über diese Queue sendet der ServerTask die SetUp-Daten
  //  an den SensorTask, dieser überträgt sie dann an den betreffenden Clienten, wenn der sich das nächste mal meldet. 
  for (;;) 
  {
    if (xQueueReceive(QueueToServerTask,PacketFromSensorTask,(TickType_t)10))  // Prüfung ob Daten in der Queue vom SensorTask vorliegen
    {
      digitalWrite(Core1_LED,LOW);                     
      if (SerialDEBUG) {Serial.print("PacketFromSensorTask: "); Serial.println(PacketFromSensorTask);}
      sServerZeit=String(now());
      LineString=sServerZeit+';'+incomingPacket;
      ServerTaskZeilenAnalyse(); // "ServerTaskDatenAnalyse.h" ... Die Clientendaten der letzten 30Tage werden aufbereitet 
      if (ChirpFile.open(ChirpFileName,FILE_WRITE)) // Datensatz in "/chirpdaten.txt" auf der SD-Karte speichern
      {
        ChirpFile.println(SaveLineString); 
        if (SerialDEBUG) {Serial.print("Gespeichert: "); Serial.println(SaveLineString);}
      }
      else
      {
        Serial.println("Filesystem _F_E_H_L_E_R_");
        Serial.println("========================");
        Serial.print(" \r\nDatei: ");
        Serial.print(ChirpFileName);
        Serial.println(" konnte für Append nicht geöffnet werden!");
        Serial.println("Neustart erfolgt!");
        Serial.println();
        sd.initErrorHalt();
        ChirpFile.close();
        delay(1000);
        ESP.restart();
      }
      ChirpFile.close();
      ServerTaskSelSensErmitteln(); // Zeiger "ServerTaskSelSens" zu den Daten des aktuellen Clienten ermitteln
      ServerTask_to_SensorTask(ServerTaskSelSens); // Daten des aktuellen Clienten in die Queue für den SensorTask ablegen
      ServerTaskDatenAnalyse(); // "ServerTaskDatenAnalyse.h" ... Die Clientendaten der letzten 30Tage werden aufbereitet 
      WebSiteErzeugen(); // "WebSiteErzeugen.h" ... Es wird die "/index.html" auf der SD-Karte erzeugt.
      digitalWrite(Core1_LED,HIGH);
    }
    server.handleClient();
	}
}

