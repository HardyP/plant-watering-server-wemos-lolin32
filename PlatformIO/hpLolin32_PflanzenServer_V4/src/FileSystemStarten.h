void BackupChirpFile()
{
  //  Erzeugt einen Backup von der "/chirpdaten.txt" auf der SD-Karte.
  //  Die Backups erhalten die Endung des betreffenden Wochentages also "/chirpdaten.so", "/chirpdaten.mo" etc.
  //  Alte Backups werden ueberschrieben.
  time_t t;
  t=now()+3600;
  strcpy(ChirpBackupFileName,"/chirpdaten.");
  if (weekday(t)==1) {strcat(ChirpBackupFileName,"so");}
  else if (weekday(t)==2) {strcat(ChirpBackupFileName,"mo");}
  else if (weekday(t)==3) {strcat(ChirpBackupFileName,"di");}
  else if (weekday(t)==4) {strcat(ChirpBackupFileName,"mi");}
  else if (weekday(t)==5) {strcat(ChirpBackupFileName,"do");}
  else if (weekday(t)==6) {strcat(ChirpBackupFileName,"fr");}
  else {strcat(ChirpBackupFileName,"sa");}
  Serial.print("Sicherungsdatei '");
  Serial.print(ChirpBackupFileName);
  Serial.println("' wird erstellt");
  if (sd.exists(ChirpBackupFileName)) {if (!sd.remove(ChirpBackupFileName)) {sd.errorHalt("FEHLER! ChirpBackupFileName konnte nicht gelöscht werden!");}}
  if (!ChirpFile.open(ChirpFileName,FILE_READ)) {sd.errorHalt("FEHLER! ChirpFileName konnte nicht geöffnet werden!");}
  if (!ChirpBackupFile.open(ChirpBackupFileName,FILE_WRITE)) {sd.errorHalt("FEHLER! ChirpBackupFileName konnte nicht erzeugt und geöffnet werden!");}
  while (ChirpFile.available()) 
    {
      CopyBufLen=ChirpFile.read(CopyBuf,sizeof(CopyBuf));
      ChirpBackupFile.write(CopyBuf,CopyBufLen);
    }
  FileSize=ChirpBackupFile.fileSize();
  Serial.print(FileSize); 
  Serial.println(" Bytes wurden kopiert.");           
  ChirpFile.close();
  ChirpBackupFile.close();
}

void TrimChirpFile()
{
  //  Alle Datensätze die älter als 30Tage sind werden aus der "/chirpdaten.txt" entfernt, dazu
  //  wird wird die aktuelle "chirpdaten.txt" gelöscht, neu erzeugt und nur die aktuellen Datensätze 
  //  aus dem aktuellen Backup übernommen.
  int geloescht=0;
  int bearbeitet=0;
  time_t OldDateTime;
  Serial.print("Veraltete Datensätze von '");
  Serial.print(ChirpFileName);
  Serial.println("' werden entfernt");
  OldDateTime=now()-Sekunden30d;
  Serial.print("Datensätze vor");
  Serial.printf("  >> %02d.%02d.%04d-%02d:%02d <<  ",day(OldDateTime),month(OldDateTime),year(OldDateTime),hour(OldDateTime),minute(OldDateTime));          
  Serial.println("sind veraltet");
  if (sd.exists(ChirpBackupFileName)) 
  { 
    if (sd.exists(ChirpFileName)) {if (!sd.remove(ChirpFileName)) {sd.errorHalt("FEHLER! ChirpFileName konnte nicht gelöscht werden!");}}
    if (!ChirpBackupFile.open(ChirpBackupFileName,FILE_READ)) {sd.errorHalt("FEHLER! ChirpBackupFileName konnte nicht geöffnet werden!");}
    if (!ChirpFile.open(ChirpFileName,FILE_WRITE)) {sd.errorHalt("FEHLER! ChirpFileName konnte nicht erzeugt und geöffnet werden!");}
    Serial.print("Berabeitung von: '");
    Serial.print(ChirpFileName);
    Serial.println("' ");
    while (ChirpBackupFile.available()) 
    {
      bearbeitet+=1;
      ChirpBackupFile.fgets(LineBuffer,sizeof(LineBuffer));
      LineString=LineBuffer;
      int OneColonIndex=LineString.indexOf(';');
      tmp=LineString.substring(0,OneColonIndex);
      Zeit=tmp.toInt();
      if (Zeit>OldDateTime) 
      {
        ChirpFile.print(LineString);
        Serial.print("Gespeichert ");
      }
      else
      {
        Serial.print("Gelöscht ");
        geloescht+=1;
      }
      PrintDateTime(Zeit);
      Serial.print(" ---> ");
      Serial.print(LineString);
    }
    Serial.print(bearbeitet);
    Serial.print(" Datensätze bearbeitet, davon ");
    Serial.print(geloescht);
    Serial.println(" Datensätze gelöscht");
    FileSize=ChirpFile.fileSize();
    Serial.print("Dateigroesse in Bytes:");           
    Serial.println(FileSize); 
    ChirpBackupFile.close();
    ChirpFile.close();
  }
  else {sd.errorHalt("FEHLER! ChirpBackupFileName ist nicht vorhanden!");}
}

void FileSystemStarten()
{
  // Das Filesystem wird gestartet, sollte dabei ein Fehler auftreten erfolgt ein Neustart des ESP.
  // Sollte sich noch keine "/chirpdaten.txt" befinden, so wird diese erzeugt.
  // Bei korrektem Start des Filesystems wird ein Backup einer bestehenden "/chirpdaten.txt" erstellt,
  // und Datensätze die älter als 30Tage sind werden gelöscht 
  Serial.println("Filesystem starten");
  Serial.println("==================");
  if (sd.begin(chipSelect, SD_SCK_MHZ(25))) {Serial.println("Filesystem wurde gestartet!");}
  else
  {
    Serial.println(" \r\nFilesystem _F_E_H_L_E_R_");
    Serial.println("========================");
    Serial.println("Schwerwiegender Fehler beim Start der SD-Karte!");
    Serial.println("Neustart erfolgt!");
    Serial.println();
    sd.initErrorHalt();
    delay(5000);
    ESP.restart();
  }
  if (!sd.exists(ChirpFileName)) 
  {
    Serial.print(" \r\nDatei: ");
    Serial.print(ChirpFileName);
    Serial.println(" nicht gefunden");
    if (ChirpFile.open(ChirpFileName,FILE_WRITE)) 
    {
      Serial.print(" \r\nDatei: ");
      Serial.print(ChirpFileName);
      Serial.println(" wurde erzeugt");
      ChirpFile.close();
    }
    else
    {
      Serial.println(" \r\nFilesystem _F_E_H_L_E_R_");
      Serial.println("========================");
      Serial.print("Datei: ");
      Serial.print(ChirpFileName);
      Serial.println(" konnte nicht erzeugt werden");
      Serial.println("Neustart erfolgt!");
      Serial.println();
      sd.initErrorHalt();
      delay(5000);
      ESP.restart();
    }
  }
  else
  {
    BackupChirpFile();
    TrimChirpFile();
  }
}
