void ggfDEBUG()
{
  int ggfDEBUG_Retry=0;
  while (ggfDEBUG_Retry<2000) 
  {
    if (Serial.available()>0)
    {
      SerialDEBUG=true;
      break;
    }
    delay(10);
    ggfDEBUG_Retry++;
  }
}

void ServerTaskZeilenAnalyse()
{
  int Colon1=LineString.indexOf(';');
  int Colon2=LineString.indexOf(';',Colon1+1);
  int Colon3=LineString.indexOf(';',Colon2+1);
  int Colon4=LineString.indexOf(';',Colon3+1);
  int Colon5=LineString.indexOf(';',Colon4+1);
  int Colon6=LineString.indexOf(';',Colon5+1);
  int Colon7=LineString.indexOf(';',Colon6+1);
  int Colon8=LineString.indexOf(';',Colon7+1);
  int Colon9=LineString.indexOf(';',Colon8+1);
  int Colon10=LineString.indexOf(';',Colon9+1);
  int Colon11=LineString.indexOf(';',Colon10+1);
  int Colon12=LineString.indexOf(';',Colon11+1);
  tmp=LineString.substring(0,Colon1);
  Zeit=tmp.toInt();
  SensorKennung=LineString.substring(Colon1+1,Colon2);
  tmp=LineString.substring(Colon2+1,Colon3);
  Spannung=tmp.toFloat()/100;
  tmp=LineString.substring(Colon3+1,Colon4);
  Temperatur=tmp.toFloat()/10;
  tmp=LineString.substring(Colon4+1,Colon5);
  Wasser=tmp.toInt();
  tmp=LineString.substring(Colon5+1,Colon6);
  Feuchte=tmp.toInt();
  tmp=LineString.substring(Colon6+1,Colon7);
  MinFeuchte=tmp.toInt();
  tmp=LineString.substring(Colon7+1,Colon8);
  SchlafZeit=tmp.toInt();
  tmp=LineString.substring(Colon8+1,Colon9);
  WartenBisWasser=tmp.toInt();
  tmp=LineString.substring(Colon9+1,Colon10);
  WartenBisWasserS=tmp.toInt();
  tmp=LineString.substring(Colon10+1,Colon11);
  PumpZeit=tmp.toInt();
  Alias=LineString.substring(Colon11+1,Colon12);
  SaveLineString=LineString.substring(0,Colon6)+";";
}
 
void ServerTaskZeilenAnalyseSD()
{
  int Colon1=LineString.indexOf(';');
  int Colon2=LineString.indexOf(';',Colon1+1);
  int Colon3=LineString.indexOf(';',Colon2+1);
  int Colon4=LineString.indexOf(';',Colon3+1);
  int Colon5=LineString.indexOf(';',Colon4+1);
  int Colon6=LineString.indexOf(';',Colon5+1);
  tmp=LineString.substring(0,Colon1);
  Zeit=tmp.toInt();
  SensorKennung=LineString.substring(Colon1+1,Colon2);
  tmp=LineString.substring(Colon2+1,Colon3);
  Spannung=tmp.toFloat()/100;
  tmp=LineString.substring(Colon3+1,Colon4);
  Temperatur=tmp.toFloat()/10;
  tmp=LineString.substring(Colon4+1,Colon5);
  Wasser=tmp.toInt();
  tmp=LineString.substring(Colon5+1,Colon6);
  Feuchte=tmp.toInt();
}

bool MitInternetVerbinden()
{
  Serial.println("Mit Internet verbinden");
  WiFi.mode(WIFI_STA); 
  WiFi.begin(router_ssid,router_password);
  int WiFiRetry=0;
  while ((WiFi.status()!=WL_CONNECTED)&&(WiFiRetry<WiFiRetryMax)) 
  {
    delay(100);
    WiFiRetry++;
    Serial.print(WiFiRetry%10);
  }
  if(WiFiRetry>=WiFiRetryMax) 
  {
    Serial.println();
    Serial.print("Verbindung mit ");
    Serial.print(router_ssid);
    Serial.println(" konnte nicht hergestellt werden! :-( ");
    delay(100);
    return false;
  }
  Serial.print(" \r\nVerbunden mit: ");
  Serial.print(router_ssid);
  Serial.print(" ---> IP ");
  Serial.println(WiFi.localIP());
  delay(100);
  return true;
}
 
void LokalNetStarten()
{
  #ifdef UseSoftAP
  Serial.println("WiFi_AP: Lokal-Net aufspannen");
  WiFi.mode(WIFI_AP);
  WiFi.softAP(server_ssid,server_password);
  delay(1000); // Warten bis AP-Start erfolgt
  Serial.println("SoftAP einrichten ....");
  WiFi.softAPConfig(ap_ip,ap_gateway,ap_subnet);
  IPAddress SoftAP_IP=WiFi.softAPIP();
  Serial.print("SoftAP-IP ist: ");
  Serial.println(SoftAP_IP);
  #else
  Serial.println("WiFi_STA: Mit Lokal-Net verbinden");
  WiFi.mode(WIFI_STA); // WIFI_AP, WIFI_STA, WIFI_AP_STA or WIFI_OFF.
  WiFi.config(ip, gateway, subnet);
  WiFi.begin(server_ssid,server_password);
  int WiFiRetry=0;
  while ((WiFi.status()!=WL_CONNECTED)&&(WiFiRetry<WiFiRetryMax)) 
  {
    delay(100);
    WiFiRetry++;
    Serial.print(WiFiRetry%10);
  }
  if(WiFiRetry==WiFiRetryMax) 
  {
    Serial.println(" \r\n_S_Y_S_T_E_M_-_F_E_H_L_E_R_!_");
    Serial.println("=============================");
    Serial.println();
    Serial.print("Verbindung mit ");
    Serial.print(server_ssid);
    Serial.println(" konnte nicht hergestellt werden! :-( ");
    Serial.println("Neustart erfolgt!");
    Serial.println();
    delay(5000);
    ESP.restart();
  }
  delay(500);
  Serial.print(" \r\nVerbunden mit: ");
  Serial.print(server_ssid);
  Serial.print(" ---> IP ");
  Serial.println(WiFi.localIP());
  #endif
}

void WiFi_Disconnect()   
{
  WiFi.disconnect();
  WiFi.setAutoConnect(false);
  WiFi.mode(WIFI_OFF); // WIFI_AP, WIFI_STA, WIFI_AP_STA or WIFI_OFF.
  Serial.println("WiFi-Verbingung beendet");
}

void Intro()
{
  int i;
  digitalWrite(Green_LED,LOW);
  digitalWrite(Core0_LED,LOW);
  digitalWrite(Core1_LED,LOW); 
  delay(1000); 
  digitalWrite(Green_LED,HIGH);
  digitalWrite(Core0_LED,HIGH);
  digitalWrite(Core1_LED,HIGH); 
  delay(200); 
  for(i=0;i<10;i++)
  {
	
    digitalWrite(Green_LED,LOW); delay(100); 
    digitalWrite(Core0_LED,LOW); delay(100); 
    digitalWrite(Core1_LED,LOW); delay(200); 
    digitalWrite(Green_LED,HIGH); delay(100);                      
    digitalWrite(Core0_LED,HIGH); delay(100);                      
    digitalWrite(Core1_LED,HIGH); delay(200); 
  }                     
  digitalWrite(Green_LED,LOW);
  digitalWrite(Core0_LED,LOW);
  digitalWrite(Core1_LED,LOW); 
  delay(1000); 
  digitalWrite(Green_LED,HIGH);
  digitalWrite(Core0_LED,HIGH);
  digitalWrite(Core1_LED,HIGH); 
}

void BlinkSensorLedTask(void * parameter) // Task fuer BlinkSensorLed
{
    digitalWrite(Core0_LED,LOW);
    vTaskDelay(500/portTICK_PERIOD_MS);
    digitalWrite(Core0_LED,HIGH);
    vTaskDelay(200/portTICK_PERIOD_MS);
    vTaskDelete(NULL);
}

void BlinkSensorLed()
{  
  xTaskCreatePinnedToCore(BlinkSensorLedTask,"SensorLedTask",10000,NULL,7,&SensorLedTask,1);
}

void LoopFadingGreenLedTask(void * parameter) // Loop fuer Fading der gruenen LED
{
  int GreenLedFreq=5000;
  int GreenLedChannel=0;
  int GreenLedResolution=8;
  ledcSetup(GreenLedChannel,GreenLedFreq,GreenLedResolution);
  ledcAttachPin(Green_LED,GreenLedChannel);
  for (;;) 
  {    
    for (int i=70;i<=255;i++) 
    {
      ledcWrite(GreenLedChannel,i);
      vTaskDelay(30/portTICK_PERIOD_MS);
    }
    vTaskDelay(5000/portTICK_PERIOD_MS);
    for (int i=255;i>=70;i--) 
    {
      ledcWrite(GreenLedChannel,i);
      vTaskDelay(30/portTICK_PERIOD_MS);
    }
	}
}

void LoopSetupBlinkLedTask(void * parameter) // Loop fuer SetupBlinkLed
{
  for (;;) 
  {    
    digitalWrite(Green_LED,LOW);
    vTaskDelay(600/portTICK_PERIOD_MS);
    digitalWrite(Green_LED,HIGH);
    vTaskDelay(400/portTICK_PERIOD_MS);
	}
}

void StartSetupBlinkLed()
{
  xTaskCreatePinnedToCore(LoopSetupBlinkLedTask,"SetupBlinkLedTask",10000,NULL,1,&SetupBlinkLedTask,1);
}

void StopSetupBlinkLed()
{
  if(SetupBlinkLedTask!=NULL) {vTaskDelete(SetupBlinkLedTask);}
}

